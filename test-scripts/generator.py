#!/usr/bin/python

import uhal
import time

if __name__ == "__main__":
	manager = uhal.ConnectionManager("file:///home/csoare/connections.xml")
	hw = manager.getDevice("dummy.ch2")

	x = 0
	while True:
		hw.getNode("REG").write(x)
		hw.getNode("REG2").write(x * 2)
		hw.getNode("REG3").write(x * 3)
		hw.dispatch()
		x += 1
		time.sleep(1)
