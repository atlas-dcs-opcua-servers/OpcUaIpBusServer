<?xml version="1.0" encoding="UTF-8"?>
<d:design xmlns:d="http://cern.ch/quasar/Design" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" projectShortName="" xsi:schemaLocation="http://cern.ch/quasar/Design Design.xsd " author="Cristian Soare, Piotr Nikiel: ATLAS Central DCS">
  <d:class name="Device">
    <d:devicelogic/>
    <d:cachevariable name="connectionFile" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>
  		If the URI is blank (URI is one of attributes of Device configuration), then this field should point to a XML file that lists possible connections to IPBus devices. If URI is given then you should keep this field empty, because the connection file will be generated for you.
  		</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="deviceId" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>If the URI is blank (URI is one of attributes of Device configuration), then this field should point to a device listed in the connection file. If URI is given then you should keep this field empty, because the name of device will be inferred from URI.</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="uri" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString" initialStatus="OpcUa_Good">
      <d:documentation>
		URI (Uniform Resource Indicator) of your IPBus device. 
		Note that you either put the URI here, and you don't use connection file, or you keep this field blank and specify connection file and deviceId instead.
		
		An example entry would be ipbusudp-2.0://localhost:50001
	</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="buffersSize" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="OpcUa_UInt32" initialStatus="OpcUa_Good">
      <d:documentation>
  		The internal buffer is per each monitored register. The minimum value is 2, and a bigger size may be required if there is additional processing (i.e. incoming Python script) or the refresh period is low (i.e. less than 10ms).
  		</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="baseInterval" addressSpaceWrite="delegated" initializeWith="configuration" nullPolicy="nullAllowed" dataType="OpcUa_UInt32" initialStatus="OpcUa_Good">
      <d:documentation>The time interval at which the polling loop is running</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="timeoutPeriod" addressSpaceWrite="delegated" initializeWith="valueAndStatus" nullPolicy="nullAllowed" dataType="OpcUa_UInt32" initialStatus="OpcUa_Good" initialValue="1000">
      <d:documentation>Timeout period of the connection</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="alwaysUpdate" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="OpcUa_Boolean" initialStatus="OpcUa_Good">
      <d:documentation>Determines wheteher data should be always updated or only on value chage</d:documentation>
    </d:cachevariable>
    <d:hasobjects instantiateUsing="configuration" class="DataItem">
</d:hasobjects>
    <d:hasobjects instantiateUsing="configuration" class="DataScript"/>
  </d:class>
  <d:class name="DataItem">
    <d:devicelogic/>
    <d:cachevariable name="refreshPeriod" addressSpaceWrite="delegated" initializeWith="configuration" nullPolicy="nullAllowed" dataType="OpcUa_UInt32" initialStatus="OpcUa_Good">
      <d:documentation>The refresh interval for this item. The value is in microseconds. When 0, this data item will not be polled.</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="wvalue" addressSpaceWrite="delegated" initializeWith="valueAndStatus" nullPolicy="nullAllowed" dataType="UaVariant" initialStatus="OpcUa_Good">
      <d:documentation>Variable through which the user can write data to the register; used only with writingDirection=true</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="rvalue" addressSpaceWrite="forbidden" initializeWith="valueAndStatus" nullPolicy="nullAllowed" dataType="UaVariant" initialStatus="OpcUa_BadWaitingForInitialData">
      <d:documentation>Variable in which the current register value will be shown; used only with writingDirection=false</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="writingDirection" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullForbidden" dataType="OpcUa_Boolean">
      <d:documentation>Determines the data flow direction</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="nodeId" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullForbidden" dataType="UaString">
      <d:documentation>NodeId of the register to be monitored</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="address" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString" initialStatus="OpcUa_Good">
      <d:documentation>Address of the register to be monitored; used only if with valid Device uri</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="type" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>Type of register data</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="incomingScript" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>Path to python script file for incoming data handling</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="outgoingScript" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>Path to python script for outgoing data handling</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="incomingFuncName" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>Name of the python function for incoming data handling</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="outgoingFuncName" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>Name of the python function for outgoing data handling</d:documentation>
    </d:cachevariable>
  </d:class>
  <d:class name="DataScript">
    <d:devicelogic/>
    <d:cachevariable name="path" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>Path to independent python script</d:documentation>
    </d:cachevariable>
    <d:cachevariable name="funcName" addressSpaceWrite="forbidden" initializeWith="configuration" nullPolicy="nullAllowed" dataType="UaString">
      <d:documentation>Name of the independent python function to be run</d:documentation>
    </d:cachevariable>
    <d:hasobjects instantiateUsing="configuration" class="DataScriptVariable"/>
    <d:cachevariable name="lastPythonExceptionAsText" addressSpaceWrite="forbidden" initializeWith="valueAndStatus" nullPolicy="nullAllowed" dataType="UaString" initialStatus="OpcUa_Good">
</d:cachevariable>
  </d:class>
  <d:class name="DataScriptVariable">
    <d:devicelogic/>
    <d:cachevariable name="value" addressSpaceWrite="forbidden" initializeWith="valueAndStatus" nullPolicy="nullAllowed" dataType="OpcUa_UInt32" initialStatus="OpcUa_BadWaitingForInitialData">
  	</d:cachevariable>
  </d:class>
  <d:root>
    <d:hasobjects instantiateUsing="configuration" class="Device"/>
  </d:root>
</d:design>
