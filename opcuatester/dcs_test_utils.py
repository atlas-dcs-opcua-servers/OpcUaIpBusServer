import os
import markup
import thread
import time
import subprocess
import pickle
import sys
import dcs_sim_connection
import random
from datetime import datetime
from optparse 		    import OptionParser

# Constants ?? // TODO: maybe they shall be somehow modifiable? Initializer function
server_path='/opt/OpcUaCanOpenServer/bin/OpcUaCanOpenServer'


simulator_path='/opt/VirtualElmbSimulator/SocketCan'
output_path='output'
options=None

server_return_code=0
server_running=False
server_crashed=False
server_was_started=False

simulator_running=False
simulator_crashed=False
valgrind_used=False

details=[]

valgrind_args=" --leak-check=full --track-origins=yes --log-file=valgrind.txt --xml=yes  "

def server_thread(cmd):
	global server_running

	server_running=True
	server_return_code=os.system(cmd)
	server_running=False
	print ('server_thread exit')

def simulator_thread():
	global simulator_running
	try:
		cmd=simulator_path+' --bus_shift='+str(options.sim_bus_shift)+' --all'
		simulator_running=True
		os.system(cmd)
		simulator_running=False
	except Exception as e:
		print ('simulator_thread excepton:'+str(e))
	print ('simulator_thread exit')

def is_server_running():
	ret=os.system('../../utils/is_server_running.sh')
	print 'Return given from scipt is'+str(ret)
	if (ret!=0):
		return True
	else:
		return False


def start_server(run_options=dict()):
	global server_was_started
	global server_crashed
	
	# check - maybe server is running and shall be killed
	if is_server_running():
		os.system('killall OpcUaCanOpenServer')
		time.sleep(5)
	else:
		if server_was_started:
			server_crashed=True
	
	#Depending of type of the interface used.
	os.system(' ../../utils/substitute_interface_type.sh ./OPCUACANOpenServer.xml.source '+options.can_interface+' ./OPCUACANOpenServer.xml')
	
	cmd=server_path+' '+os.getcwd()+'/OPCUACANOpenServer.xml'
	if options.valgrind:
		print "IMPORTANT: running under valgrind"
		global valgrind_args,valgrind_used
		valgrind_args = valgrind_args + "--xml-file=../../output/valgrind-out-"+options.output+".xml "
		cmd="/usr/bin/valgrind "+valgrind_args+cmd
		valgrind_used=True
	thread.start_new_thread (server_thread, (cmd,))
	server_was_started=True
	# need to run the simulator separately????
	if options.can_interface != 'simulated':
		os.system('killall SocketCan')
		thread.start_new_thread (simulator_thread, ())
	time.sleep(3)
	dcs_sim_connection.open_connection()
	return server_running


def kill_server():
	global server_crashed

	if options.log_frames:
		dcs_sim_connection.dump_frames_log()
		time.sleep(5)
		

	if server_running:
		os.system('pkill -f OpcUaCanOpenServer')
	else:
		server_crashed=True
	if options.can_interface != 'simulated':
		os.system('killall SocketCan')

last_log_time=None

def log_detail(status,message,location):
	global last_log_time
	tnow=datetime.now()
	tdiff=0
	if last_log_time!=None:
		tdelta=tnow-last_log_time
		tdiff = tdelta.microseconds/1000.0 + tdelta.seconds*1000.0
	last_log_time=tnow
	details.append([status,message,location,tdiff])
def log_comment(message):
	log_detail(None,message,'')


def get_summary():
	bad=0
	for d in details:
		if d[0]==False: bad=bad+1
	return(len(details),bad)

def create_detailed_html():
	page=markup.page()
	page.init(title='Detailed results from ...', header='done by '+os.environ['USER']+' on '+str(datetime.now()), footer='xxx')
	page.p('Server crashed='+str(server_crashed))
	opt_s=''
	for a in sys.argv:
		opt_s=opt_s+a+' '
	page.p('supplied options: '+opt_s)


	(all,bad) = get_summary()
	print_only_bad=False
	max_lines=1000
	if options.limit_detailed_reports:
		if all>max_lines:
			page.p ('<b>WARNING</b> More than 1000 result lines recorded.  Printing only bad lines. Tdelta is inaccurate.')
			print_only_bad=True
 


	page.table(cols='3', rows='2', border="1")
	page.tr()
	
	page.td('Tdelta [ms]')
	page.td.close()
	page.td('Result', width=100)
	page.td('Details')
	page.td('Location')
	page.tr.close()


			 	

	i=0
	for d in details:

		if not print_only_bad or (print_only_bad and not d[0]):			
			page.tr()
			page.td("%6.1f"%d[3],align='right')
			page.td.close()		
			if d[0]==None:
				page.td("COMMENT",bgcolor='SeaShell')
			elif (d[0]):
				page.td("OK",bgcolor='SpringGreen')
			else:
				page.td("Bad_Bad",bgcolor='Red')
			page.td(d[1])
			page.td.close()
			if (len(d) >= 3):
				page.td(d[2])
			page.tr.close()
			i=i+1
		if options.limit_detailed_reports and i>max_lines:
			page.tr()
			page.td('-- report cut due to too many records. Remeber that print_only_bad='+str(print_only_bad)+' --')
			page.td.close()
			page.tr.close()
			break
			

	f=open('../../'+output_path+'/'+options.output+'.html','w')
	f.write(str(page))
	f.close()

	


def create_summary_pickle(output=None):
	data={}
	f=open('../../'+output_path+'/'+options.output+'.pickle','w')
	data['server_crashed']=server_crashed
	(all,bad)=get_summary()
	data['num_all']=all
	data['num_bad']=bad
	pickle.dump(data,f)


def get_standard_parser():
	parser = OptionParser()
	parser.add_option ("--output", dest="output", default="output")
	parser.add_option ("--can_interface", dest="can_interface", default="simulated")
	parser.add_option ("--sim_bus_shift", dest="sim_bus_shift", default=0, type="int")
	parser.add_option ("--log_frames", dest="log_frames", action="store_true")
	parser.add_option ("--limit_detailed_reports", dest="limit_detailed_reports", action="store_true")
	parser.add_option ("--valgrind", dest="valgrind", action="store_true")
	return parser

def create_range_with_probability(fr,to,probabl):
	out=[]
	for i in range(fr,to):
		if random.random() < probabl:
			out.append(i)
	return out


def parse_options(parser=None):
	global options
	if parser==None:
		parser=get_standard_parser()
	(options,args) = parser.parse_args()
	

