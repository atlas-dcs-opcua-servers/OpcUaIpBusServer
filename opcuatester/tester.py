import dcs_opc_ua_elmb
import time
from pyuaf.util.primitives import *



if __name__ == "__main__":
	dcs_opc_ua_elmb.connect()

	success_count = 0
	all_count = 0

	# floating point 2.3
	dcs_opc_ua_elmb.client.write([dcs_opc_ua_elmb.addr('device.wd.wvalue')], [UInt32(1075000115)])
	time.sleep(0.2)
	res = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device.rd_float.rvalue')])
	print "testing float 2.3 got " + str(res.targets[0].data)
	if res.targets[0].data == Float(2.3):
		success_count += 1
	all_count += 1

	# uint8 25
	dcs_opc_ua_elmb.client.write([dcs_opc_ua_elmb.addr('device.wd.wvalue')], [UInt32(25)])
	time.sleep(0.2)
	res = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device.rd_uint8le.rvalue')])
	print "testing uint8 25 got " + str(int(res.targets[0].data.value))
	if res.targets[0].data == Byte(25):
		success_count += 1
	all_count += 1

	# uint16 32
	dcs_opc_ua_elmb.client.write([dcs_opc_ua_elmb.addr('device.wd.wvalue')], [UInt32(32)])
	time.sleep(0.2)
	res = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device.rd_uint16le.rvalue')])
	print "testing 32  " + str(res.targets[0].data)
	if res.targets[0].data == UInt16(32):
		success_count += 1
	all_count += 1

	# # uint32 128
	dcs_opc_ua_elmb.client.write([dcs_opc_ua_elmb.addr('device.wd.wvalue')], [UInt32(128)])
	time.sleep(0.2)
	res = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device.rd.rvalue')])
	print "testing 128 got " + str(res.targets[0].data)
	if res.targets[0].data == UInt32(128):
		success_count += 1
	all_count += 1

	print str(success_count) + " out of " + str(all_count) + " tests passed"
