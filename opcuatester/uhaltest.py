import dcs_opc_ua_elmb
import time
from pyuaf.util.primitives import *

import uhal



if __name__ == "__main__":
	connectionFile = "file:///home/csoare/connections.xml"
	deviceId = "myfpga"

	dcs_opc_ua_elmb.connect()
	manager = uhal.ConnectionManager(connectionFile)
	hw = manager.getDevice(deviceId)


	success_count = 0
	all_count = 0

	# floating point 2.3 read
	hw.getNode("REG").write(1075000115)
	hw.dispatch()
	res = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device.rd_float.rvalue')])
	if res.targets[0].data == Float(2.3):
		success_count += 1
	all_count += 1

	dcs_opc_ua_elmb.client.write([dcs_opc_ua_elmb.addr('device.wd.wvalue')], [Float(2.3)])
	res = hw.getNode("REG").read()
	hw.dispatch()
	if res.value() == 1075000115:
		success_count += 1
	all_count += 1

	print str(success_count) + " out of " + str(all_count) + " tests passed"

