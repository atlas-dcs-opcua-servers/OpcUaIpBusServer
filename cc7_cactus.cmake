#OPC UA Toolkit
SET( OPCUA_TOOLKIT_PATH "/opt/OpcUaToolkit-static-1.5.5" )
SET( OPCUA_TOOLKIT_LIBS_DEBUG "-luamoduled -lcoremoduled -luabased -luastackd -luapkid -lxmlparserd -lxml2 -lssl -lcrypto -lpthread -lrt" ) 
SET( OPCUA_TOOLKIT_LIBS_RELEASE "-luamodule -lcoremodule -luabase -luastack -luapki -lxmlparser -lxml2 -lssl -lcrypto -lpthread -lrt" ) 

#Boost
SET( BOOST_PATH_HEADERS "" )
SET( BOOST_PATH_LIBS "" )
SET( BOOST_LIBS "-lboost_regex-mt -lboost_chrono-mt -lboost_program_options-mt -lboost_thread-mt -lboost_system-mt -lboost_python-mt")

#XML
SET( XML_LIBS "-lxerces-c" ) 

#Other options
include_directories(
        /usr/include/python2.7
        /opt/cactus/include
        )
add_definitions(-std=gnu++0x -Wall -DBACKEND_UATOOLKIT )
 
