import time
import pyuaf
from pyuaf.util             import Address, NodeId, opcuaidentifiers
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings, SessionSettings
from pyuaf.util             import loglevels
import dcs_test_utils

#Constants (??)

server_address='opc.tcp://localhost:4843'
server_uri='urn:ATLASDCS:OpcUaIpBusServer'
server_ns=2

client = []
def connect():
	global client
	cs = ClientSettings("myClient", [server_address])
# Uncomment below for session logging
#	cs.logToStdOutLevel = loglevels.Info 
	max_attempts=20
	num_attempts=0
	while (True):
		result=None
		try:
			print 'Trying to connect to OPC UA'
			client = Client(cs)
			rootNode = Address( NodeId(opcuaidentifiers.OpcUaId_RootFolder, 0), server_uri )
			result=client.browse ([ rootNode ])
		except Exception as e:
			num_attempts = num_attempts+1
			if num_attempts > max_attempts:
				msg = 'Despite '+str(max_attempts)+ ' attempts couldnt establish OPC UA connection: exception is '+str(e)
				print msg
				dcs_test_utils.log_detail(False,msg,'')
				raise Exception(msg)
			time.sleep(3)
			continue
		print 'Connected to OPC UA Server'
		return
			
		

def addr(sa):
	return Address(NodeId(sa,server_ns), server_uri)

def addr_to_str(address):
	return address.getExpandedNodeId().nodeId().identifier().idString

