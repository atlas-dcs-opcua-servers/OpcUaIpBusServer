# pylint: disable=line-too-long
'''
Created on Sep 19, 2015

@author: pnikiel

IpBus server mechanisms used:
- monitoring of registers
- "incoming" scripts
- "outgoing" scripts
'''
from __future__ import print_function
import sys
import dcs_opc_ua_elmb
import random
from pyuaf.util.primitives import UInt32
from IpBusTestEnvironment import IpBusTestEnvironment
from TestModule import TestModule
import uhal
import time
import traceback



class TonsOfRegisters(TestModule, IpBusTestEnvironment):
    '''
    Test writing and reading using different mechanisms:
    - wvalue
    - rvalue
    - incoming script
    - outgoing script
    - independent script writing
    - independent script reading
    '''

    def setup_uhal(self, num_devices):
        ''' Thanks to that, a write() or read() method can be called on uhal devices. '''
        self.uhal_manager = uhal.ConnectionManager("file://connections.xml")
        self.uhal_devices = [None] * num_devices
        for dev_i in range(0, num_devices):
            self.uhal_devices[dev_i] = self.uhal_manager.getDevice('dev_'+str(dev_i))
    def make_server_config_file(self, num_devices, num_registers):
        ''' Write IpBus server XML config file. '''
        print('Creating config file', file=sys.stderr)
        out = open('config.xml', 'w')
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        out.write('<tns:configuration xmlns:tns="http://cern.ch/quasar/Configuration" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://cern.ch/quasar/Configuration '+self.config_stylesheet()+' ">\n')
        for device_i in range(0, num_devices):
            out.write('<tns:Device name="device'+str(device_i)+'" deviceId="" connectionFile="" uri="ipbusudp-2.0://localhost:'+str(self.simulator_devno_to_port(device_i))+'" alwaysUpdate="true" buffersSize="100" baseInterval="100" >\n')
            for reg_i in range(0, num_registers):
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'" incomingScript="" nodeId="REG_'+str(reg_i)+'" incomingFuncName="" writingDirection="false"'
                          ' refreshPeriod="1000" type="uint32le" outgoingFuncName="" outgoingScript="" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'_py" incomingScript="scripts.py" nodeId="REG_'+str(reg_i)+'" incomingFuncName="incoming" writingDirection="false"'
                          ' refreshPeriod="1000" type="uint32le" outgoingFuncName="" outgoingScript="" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'_w" incomingScript="" nodeId="REG_'+str(reg_i)+'" incomingFuncName="" writingDirection="true"'
                          ' refreshPeriod="0" type="uint32le" outgoingFuncName="" outgoingScript="" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'_w_py" incomingScript="" nodeId="REG_'+str(reg_i)+'" incomingFuncName="" writingDirection="true"'
                          ' refreshPeriod="0" type="uint32le" outgoingFuncName="outgoing" outgoingScript="scripts.py" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
            out.write('<tns:DataScript name="independent_script" path="scripts.py" funcName="independent_script">\n')
            for reg_i in range(0, num_registers):
                out.write('<tns:DataScriptVariable name="v'+str(reg_i)+'"/>\n')
            out.write('</tns:DataScript>\n')
            out.write('</tns:Device>\n')
        out.write('</tns:configuration>')
        out.close()
    def make_connections_addresses_files(self, num_devices, num_registers):
        '''
        Assumption: create one connection file which has <num_devices> entries, all pointing to the same address file.
        '''
        out = open('connections.xml', 'w')
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        out.write('<connections>\n')
        for dev_i in range(0, num_devices):
            out.write('<connection id="dev_'+str(dev_i)+'" uri="ipbusudp-2.0://localhost:'+str(self.simulator_devno_to_port(dev_i))+'" address_table="file://addresses.xml" />\n')
        out.write('</connections>\n')
        out.close()
        out = open('addresses.xml', 'w')
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        out.write('<node id="TOP">\n')
        for reg_i in range(0, num_registers):
            out.write('<node id="REG_'+str(reg_i)+'" address="0x'+str(reg_i)+'" permission="rw"/>\n')
        out.write('</node>\n')
        out.close()
    def make_random_data(self, num_devices, num_registers):
        test_data = [[random.randint(0, 2000000) for i in range(num_registers)] for j in range(num_devices)]
        return test_data
    def write_to_device_opcua_straight(self, dev_i, reg_i, value):
        ''' Writes without outgoing script '''
        self.get_client().write([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_w.wvalue')], [value])
    def write_to_device_opcua_py(self, dev_i, reg_i, value):
        ''' Writes with outgoing script '''
        self.get_client().write([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_w_py.wvalue')], [value])
    def read_from_device_uhal(self, dev_i, reg_i):
        ''' Reads without OPC UA '''
        uhal_device = self.uhal_devices[dev_i]
        result = uhal_device.getNode('REG_'+str(reg_i)).read()
        uhal_device.dispatch()
        return result.value()
    def write_to_device_uhal(self, dev_i, reg_i, value):
        ''' Writes without OPC UA '''
        uhal_device = self.uhal_devices[dev_i]
        uhal_device.getNode('REG_'+str(reg_i)).write(value)
        uhal_device.dispatch()
    def factual_test(self, num_devices, num_registers, num_iterations):
        self.logComment('Starting TonsOfRegisters')
        try:
            for dev_i in range(0, num_devices):
                self.start_simulator(dev_i, 'udp')
            self.make_connections_addresses_files(num_devices, num_registers)
            self.setup_uhal(num_devices)
            self.make_server_config_file(num_devices, num_registers)
            self.start_server('config.xml')
            self.connect_opcua()
            time.sleep(6)
            for iter_i in range(0, num_iterations):
                self.logComment('Iteration '+str(iter_i))
                print ('Starting iteration '+str(iter_i))
                test_data = self.make_random_data(num_devices, num_registers)
                # phase 1. write to device memory through OPC-UA using wvalue and no python processing in between, and check using pyuhal
                self.logComment('Phase 1 -- OPC-UA write to dev memory (no outgoing script), pyuhal check')
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        self.write_to_device_opcua_straight(dev_i, reg_i, UInt32(test_data[dev_i][reg_i]))
                time.sleep(2) # propagation delay
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        result = self.read_from_device_uhal(dev_i, reg_i)
                        self.assertEqual(result, test_data[dev_i][reg_i], 'Written through OPC-UA (no outgoing script), read through uhal', str(dev_i)+'/'+str(reg_i))
                # phase 2. write to device memory through OPC-UA using wvalue with python processing (outgoing script) and check using pyuhal
                self.logComment('Phase 2 -- OPC-UA write to dev memory (with outgoing script), pyuhal check')
                test_data = self.make_random_data(num_devices, num_registers)
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        self.write_to_device_opcua_py(dev_i, reg_i, UInt32(test_data[dev_i][reg_i]))
                time.sleep(2) # propagation delay
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        result = self.read_from_device_uhal(dev_i, reg_i)
                        self.assertEqual(result, 2*test_data[dev_i][reg_i]+1, 'Written through OPC-UA (through outgoing script 2x+1 ), read through uhal', str(dev_i)+'/'+str(reg_i))
                # phase 3. write to device memory through pyuhal and read through OPC-UA without python processing
                self.logComment('Phase 3 -- pyuhal write to dev memory, OPC-UA read (no incoming script)')
                test_data = self.make_random_data(num_devices, num_registers)
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        self.write_to_device_uhal(dev_i, reg_i, test_data[dev_i][reg_i])
                time.sleep(3)
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        read_value = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'.rvalue')])
                        self.assertEqual(read_value.targets[0].data.value, test_data[dev_i][reg_i], 'Written through uhal, read through OPC-UA (no incoming script)', str(dev_i)+'/'+str(reg_i))
                # phase 4. write to device memory through pyuhal and read through OPC-UA with python processing
                self.logComment('Phase 4 -- pyuhal write to dev memory, OPC-UA read (with incoming script)')
                test_data = self.make_random_data(num_devices, num_registers)
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        self.write_to_device_uhal(dev_i, reg_i, test_data[dev_i][reg_i])
                time.sleep(3)
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        read_value = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_py.rvalue')])
                        self.assertEqual(read_value.targets[0].data.value, 7+3*test_data[dev_i][reg_i], 'Written through uhal, read through OPC-UA (with incoming script)', str(dev_i)+'/'+str(reg_i))
                # phase 5. see if the data copied using independent script matches data written using uhal
                self.logComment('Phase 5 -- pyuhal write to dev memory, independent script copy, read using OPC-UA')
                for dev_i in range(0, num_devices):
                    for reg_i in range(0, num_registers):
                        read_value = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.independent_script.v'+str(reg_i)+'.value')])
                        if read_value.targets[0].data is None:
                            #pdb.set_trace()
                            self.assertTrue(False, 'value is NULL')
                        else:
                            self.assertEqual(read_value.targets[0].data.value, test_data[dev_i][reg_i], 'Written through uhal, read data copied using independent script through OPC-UA', str(dev_i)+'/'+str(reg_i))
            print ('Test finished, now trying to gracefully stop the server and simulator(s)...')
        except Exception as exception:
            print ('Got exception, shutting down the test infrastructure ...')
            traceback.print_exc()
            raise exception
            # TODO: notify suite that it failed
        finally:
            time.sleep(5)
            self.stop_server()
            time.sleep(2)
            self.kill_simulators()
    def test_1dev_100regs(self):
        print('spam', file=sys.stderr)
        self.factual_test(10, 100, 10)
        
