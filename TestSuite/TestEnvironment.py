from multiprocessing import Process
import os
import dcs_opc_ua_elmb
import time

class TestEnvironment:
    '''
    Provides base for:
    - starting/stopping server under test,
    - starting/stopping simulator(s)
    
    For running server and simulator(s) the Process API is used. For killing them, the Unix utility 'pkill' is used.
    Note that Process.terminate() can't be used because it can not reliably kill children processes (they rather get orphaned).
    '''
    
    def __init__(self, path_server, server_pkill_pattern):
        self.path_server = path_server
        self.server_pkill_pattern = server_pkill_pattern
        
    def application_thread(self, command):
        '''
        This thread hosts server, simulator(s) or any other thing that should be run as a sub-process.
        '''
        print ('os.system '+command+' begin, err goes to err.txt')
        os.system(command + ' > err.txt ')
        print ('os.system '+command+' end')    
    
    def start_server(self, args):
        '''
        args arguments to the server, i.e. config file
        '''
        self.server_process = Process(target=TestEnvironment.application_thread, args=(self, self.path_server+' '+args,))
        self.server_process.start()
        time.sleep(1)
        if not self.server_process.is_alive():
            raise Exception ('Failed to start the server.')
        
    def stop_server(self):
        rv = os.system('pkill '+self.server_pkill_pattern)
        print ('pkill '+self.server_pkill_pattern+' returned '+str(rv))
    
    def connect_opcua(self):
        dcs_opc_ua_elmb.connect()
    
    def get_client(self):
        return dcs_opc_ua_elmb.client
