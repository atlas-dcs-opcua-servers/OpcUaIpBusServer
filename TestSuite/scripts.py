import time

num_of_registers = 2

def outgoing(a):
	a.write(2*a.value+1)

def outgoing_throws(a):
	raise Exception('A test exception')

def incoming(raw_value):
	return 3*raw_value+7

def incoming_throws(raw_value):
        raise Exception('A test exception')

def independent_script(a):
	output_variables = a.getOutputVariables()
	while a.ongoing:
		for i in range(0,len(output_variables)):
			if not a.ongoing:
				return
			x = a.readReg('REG_'+str(i))
			a.storeOutput('v'+str(i), x)
		time.sleep(0.1)
	pass
