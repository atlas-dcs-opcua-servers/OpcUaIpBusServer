# pylint: disable=line-too-long
'''
Created on Jul 14, 2016

@author: pnikiel

IpBus server mechanisms used:
- error notifications
'''
from __future__ import print_function
import sys
import dcs_opc_ua_elmb
import random
from pyuaf.util.primitives import UInt32
from pyuaf.util.statuscodes import Good
from IpBusTestEnvironment import IpBusTestEnvironment
from TestModule import TestModule
import uhal
import time
import traceback



class DeviceGoesDown(TestModule, IpBusTestEnvironment):
    '''
    Test writing and reading using different mechanisms:
    - wvalue
    - rvalue
    - incoming script
    - outgoing script
    - independent script writing
    - independent script reading
    '''

    def setup_uhal(self, num_devices):
        ''' Thanks to that, a write() or read() method can be called on uhal devices. '''
        self.uhal_manager = uhal.ConnectionManager("file://connections.xml")
        self.uhal_devices = [None] * num_devices
        for dev_i in range(0, num_devices):
            self.uhal_devices[dev_i] = self.uhal_manager.getDevice('dev_'+str(dev_i))
    def make_server_config_file(self, num_devices, num_registers):
        ''' Write IpBus server XML config file. '''
        print('Creating config file', file=sys.stderr)
        out = open('config.xml', 'w')
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        out.write('<tns:configuration xmlns:tns="http://cern.ch/quasar/Configuration" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://cern.ch/quasar/Configuration '+self.config_stylesheet()+' ">\n')
        for device_i in range(0, num_devices):
            out.write('<tns:Device name="device'+str(device_i)+'" deviceId="" connectionFile="" uri="ipbusudp-2.0://localhost:'+str(self.simulator_devno_to_port(device_i))+'" alwaysUpdate="true" buffersSize="100" baseInterval="100" >\n')
            for reg_i in range(0, num_registers):
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'" incomingScript="" nodeId="REG_'+str(reg_i)+'" incomingFuncName="" writingDirection="false"'
                          ' refreshPeriod="1000" type="uint32le" outgoingFuncName="" outgoingScript="" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'_py" incomingScript="scripts.py" nodeId="REG_'+str(reg_i)+'" incomingFuncName="incoming" writingDirection="false"'
                          ' refreshPeriod="1000" type="uint32le" outgoingFuncName="" outgoingScript="" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'_py_exception" incomingScript="scripts.py" nodeId="REG_'+str(reg_i)+'" incomingFuncName="incoming_throws" writingDirection="false"'
                          ' refreshPeriod="1000" type="uint32le" outgoingFuncName="" outgoingScript="" address="0x'+str(reg_i)+'"></tns:DataItem>\n')               
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'_w" incomingScript="" nodeId="REG_'+str(reg_i)+'" incomingFuncName="" writingDirection="true"'
                          ' refreshPeriod="0" type="uint32le" outgoingFuncName="" outgoingScript="" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'_w_py" incomingScript="" nodeId="REG_'+str(reg_i)+'" incomingFuncName="" writingDirection="true"'
                          ' refreshPeriod="0" type="uint32le" outgoingFuncName="outgoing" outgoingScript="scripts.py" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
                out.write('<tns:DataItem name="REG_'+str(reg_i)+'_w_py_exception" incomingScript="" nodeId="REG_'+str(reg_i)+'" incomingFuncName="" writingDirection="true"'
                          ' refreshPeriod="0" type="uint32le" outgoingFuncName="outgoing_throws" outgoingScript="scripts.py" address="0x'+str(reg_i)+'"></tns:DataItem>\n')
                
            out.write('<tns:DataScript name="independent_script" path="scripts.py" funcName="independent_script">\n')
            for reg_i in range(0, num_registers):
                out.write('<tns:DataScriptVariable name="v'+str(reg_i)+'"/>\n')
            out.write('</tns:DataScript>\n')
            out.write('</tns:Device>\n')
        out.write('</tns:configuration>')
        out.close()
    def make_connections_addresses_files(self, num_devices, num_registers):
        '''
        Assumption: create one connection file which has <num_devices> entries, all pointing to the same address file.
        '''
        out = open('connections.xml', 'w')
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        out.write('<connections>\n')
        for dev_i in range(0, num_devices):
            out.write('<connection id="dev_'+str(dev_i)+'" uri="ipbusudp-2.0://localhost:'+str(self.simulator_devno_to_port(dev_i))+'" address_table="file://addresses.xml" />\n')
        out.write('</connections>\n')
        out.close()
        out = open('addresses.xml', 'w')
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        out.write('<node id="TOP">\n')
        for reg_i in range(0, num_registers):
            out.write('<node id="REG_'+str(reg_i)+'" address="0x'+str(reg_i)+'" permission="rw"/>\n')
        out.write('</node>\n')
        out.close()
    def make_random_data(self, num_devices, num_registers):
        test_data = [[random.randint(0, 2000000) for i in range(num_registers)] for j in range(num_devices)]
        return test_data
    def write_to_device_opcua_straight(self, dev_i, reg_i, value):
        ''' Writes without outgoing script '''
        self.get_client().write([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_w.wvalue')], [value])
    def write_to_device_opcua_py(self, dev_i, reg_i, value):
        ''' Writes with outgoing script '''
        self.get_client().write([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_w_py.wvalue')], [value])
    def read_from_device_uhal(self, dev_i, reg_i):
        ''' Reads without OPC UA '''
        uhal_device = self.uhal_devices[dev_i]
        result = uhal_device.getNode('REG_'+str(reg_i)).read()
        uhal_device.dispatch()
        return result.value()
    def write_to_device_uhal(self, dev_i, reg_i, value):
        ''' Writes without OPC UA '''
        uhal_device = self.uhal_devices[dev_i]
        uhal_device.getNode('REG_'+str(reg_i)).write(value)
        uhal_device.dispatch()
    def factual_test(self, num_devices, num_registers, num_iterations):
        self.logComment('Starting DeviceGoesDown')
        try:
            for dev_i in range(0, num_devices):
                self.start_simulator(dev_i, 'udp')
            self.make_connections_addresses_files(num_devices, num_registers)
            self.setup_uhal(num_devices)
            self.make_server_config_file(num_devices, num_registers)
            self.start_server('config.xml')
            self.connect_opcua()
            time.sleep(30)

            


            # fix dev and reg for a moment
            dev_i = 0
            reg_i = 0

            # Point 1 of test plan: 1) When connection is lost on some device, or all of them, respective inputs should be bad (without incoming script)
            # check some values to be OK
            read_value = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'.rvalue')])
            # status should be good
            self.assertTrue(read_value.targets[0].status.isGood(), 'Status good before killing simulator')
            self.kill_simulator(0)
            time.sleep(10)
            read_value = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'.rvalue')])
            self.assertFalse(read_value.targets[0].status.isGood(), 'Status after killing simulator: '+str(read_value.targets[0].status))

            # Point 2 of test plan: 2) When connection is lost on some device, or all of them, respective inputs should be bad (with incoming script)
            read_value = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_py.rvalue')])
            self.assertFalse(read_value.targets[0].status.isGood(), 'Incoming script status on broken register '+str(read_value.targets[0].status))


            
            
            # Point 3 of test plan: 3) Writing when connection is broken should returned not good
            w_status = dcs_opc_ua_elmb.client.write([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_w.wvalue')], [UInt32(69)])
            
            self.assertEqual(w_status.targets[0].status.opcUaStatusCode(), 0x80050000, 'Writing to a dead device: is it a bad: communication error?', '')
            # TODO: the server for now returns just Bad, not BadCommunicationError

            # Point 4 of test plan: Thrown exception in outgoing script should give bad write status
            dev_i = 1  # should do on this one because we killed the one from dev 0
            w_status = dcs_opc_ua_elmb.client.write([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_w_py_exception.wvalue')], [UInt32(69)])
            self.assertFalse(w_status.targets[0].status.isGood(), 'Writing when outcoming throws: should be bad')
            

            # Point 5 of test plan: Thrown exception in incoming script should give bad status
            dev_i = 1  # should do on this one because we killed the one from dev 0
            read_value = dcs_opc_ua_elmb.client.read([dcs_opc_ua_elmb.addr('device'+str(dev_i)+'.REG_'+str(reg_i)+'_py_exception.rvalue')])
            self.assertFalse(read_value.targets[0].status.isGood(), 'Status from register whose py script throws: '+str(read_value.targets[0].status))

            
            
            print ('Test finished, now trying to gracefully stop the server and simulator(s)...')
        except Exception as exception:
            print ('Got exception, shutting down the test infrastructure ...')
            traceback.print_exc()
            raise exception
            # TODO: notify suite that it failed
        finally:
            time.sleep(5)
            self.stop_server()
            time.sleep(2)
            self.kill_simulators()
    def test_1dev_100regs(self):
        print('spam', file=sys.stderr)
        self.factual_test(2, 2, 1)
        
