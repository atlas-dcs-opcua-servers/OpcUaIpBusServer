import dcs_test_utils

class TestModule():
    def assertTrue(self, v, message):
        dcs_test_utils.log_detail(v == True, message, '')
    def assertFalse(self, v, message):
        dcs_test_utils.log_detail(v == False, message, '')
    def assertEqual(self, v, expected, message, loc=''):
        if v == expected:
            dcs_test_utils.log_detail(True, 'Value matches, '+message, loc)
        else:
            dcs_test_utils.log_detail(False, 'Obtained:'+str(v)+' expeced:'+str(expected)+' '+message, loc)
    def logComment(self, message):
        dcs_test_utils.log_comment(message)