from TestEnvironment import TestEnvironment
from multiprocessing import Process
import time
import os
import subprocess

class IpBusTestEnvironment(TestEnvironment):
    '''
    Concretization of TestEnvironment for OpcUaIpBus server
    
    Adds infrastructure for running IpBus uhal simulator(s).
    Note there might be many simulators running simultaneously as 1 simulator is needed for 1 device.
    '''
    def __init__(self):
        TestEnvironment.__init__(self, '../bin/OpcUaIpBusServer', 'OpcUaIpBus')
        self.uhal_manager = None
        self.uhal_devices = None
        self.simulator_processes = {}
    
    def simulator_devno_to_port(self, dev_i):
        return 50000+dev_i
    
    def start_simulator(self, dev_i, tcp_udp):
        cmd = "export LD_LIBRARY_PATH=/opt/cactus/lib;"
        if tcp_udp == 'udp':
            cmd = "/opt/cactus/bin/uhal/tests/DummyHardwareUdp.exe"
        elif tcp_udp == 'tcp':
            cmd = "/opt/cactus/bin/uhal/tests/DummyHardwareTcp.exe"
        else:
            raise Exception ('Invalid argument "'+tcp_udp+'", expected tcp or udp')
        p = subprocess.Popen([cmd, '-p', str(self.simulator_devno_to_port(dev_i)), '-v', '2'])
        print 'Started simulator for device #'+str(dev_i)+', pid='+str(p.pid)
        self.simulator_processes[dev_i] = p

    def kill_simulator(self,dev_i):
        print 'Trying to terminate simulator for device #'+str(dev_i)
        p = self.simulator_processes[dev_i]
        p.kill()
        p.wait()
        print 'simulator for device #'+str(dev_i)+' killed'
        
        
    def kill_simulators(self):
        print ('Trying to kill simulators')
        rv = os.system('/usr/bin/pkill DummyHardw')
        print ('pkill returned with return value os '+str(rv))
    
        
    def config_stylesheet(self):
        return "../bin/Configuration.xsd"    
