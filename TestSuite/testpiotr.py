#!/usr/bin/python

''' This simply runs all the tests prepared for the IpBus server '''

import DeviceGoesDown
import TonsOfRegisters
import dcs_test_utils
import time

def main():
    ''' The main of the testsuite '''
    test = TonsOfRegisters.TonsOfRegisters()
    test.test_1dev_100regs()
    time.sleep(10)
    test = DeviceGoesDown.DeviceGoesDown()
    test.test_1dev_100regs()
    
    dcs_test_utils.options = {'output':'output'}
    dcs_test_utils.create_detailed_html()

if __name__ == "__main__":
    main()
