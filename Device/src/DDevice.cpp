/**
 * DDevice.cpp
 * Created on: July 16, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDevice.h>
#include <ASDevice.h>
#include <ASDataItem.h>

#include <DDataItem.h>
#include <DDataScript.h>

#include <iostream>
#include <fstream>
#include <LogIt.h>

#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/regex.hpp>
#include <sstream>

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDevice::DDevice (const Configuration::Device & config

                  , DRoot * parent

                 ):
    Base_DDevice( config

                  , parent

                )
/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
	m_connectionFile = std::string(config.connectionFile().c_str());
	m_deviceId = std::string(config.deviceId().c_str());
	m_uri = std::string(config.uri().c_str());
	m_upolicy = config.alwaysUpdate() ? Monitor::Always : Monitor::NewValue;
	m_buffersSize = config.buffersSize();
	m_baseInterval = config.baseInterval();
	if (m_baseInterval < 100)
		// TODO: achieve that through limits imposed in the XSD
		throw std::runtime_error("baseInterval smaller than 100ms is not a good idea. Please increase it.");
	m_itemsCount = 0;
}

/* sample dtr */
DDevice::~DDevice ()
{

}


// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DDevice::init()
{
	if (m_buffersSize <= 0)
		throw std::runtime_error("Buffers size for " + getFullName() + " must be greater than 0");

	// intialize monitor
	Monitor* mon = new Monitor(getNewConnection(), getFullName(), m_buffersSize);
	mon->setTimeout(1000);
	mon->setInterval(m_baseInterval);
	mon->setDevice(this);
	mon->setUpdatePolicy(m_upolicy);
	setMonitor(mon);

	if (dataitems().empty()) {
		LOG(Log::WRN) << "no data items configured";
		return;
	}

	// initialize data items
	m_itemsCount = 0;
	BOOST_FOREACH(DDataItem* di, dataitems()) {
		subscribe(di->getNodeId());
		di->setMonitor(m_monitor);
		m_opcUaIdx.push_back(m_itemsCount);
		m_itemsCount++;

		unsigned int x = 0;
		UaVariant val;
		Converter::convert((unsigned char*)&x, val, Converter::getType(di->getType()));
		di->m_addressSpaceLink->setWvalue(val, OpcUa_Good, UaDateTime::now());
	}

	BOOST_FOREACH(DDataScript* ds, datascripts()) {
		ds->init(m_monitor);
	}

	m_ongoing = true;
	m_thr = boost::thread(&DDevice::run, this);
	m_monitor->start();
}

void DDevice::destroy()
{
	m_ongoing = false;
	if (m_monitor)
		m_monitor->stop();

	if (m_thr.joinable())
		m_thr.join();
	delete m_monitor;

	BOOST_FOREACH(DDataScript* ds, datascripts()) {
		ds->destroy();
	}
}

/* delegators for cachevariables and externalvariables */

UaStatus DDevice::writeBaseInterval ( const OpcUa_UInt32 & v)
{
	m_monitor->setInterval(v);
	return OpcUa_Good;
}

UaStatus DDevice::writeTimeoutPeriod ( const OpcUa_UInt32 & v)
{
	m_monitor->setTimeout(v);
    return OpcUa_Good;
}

uhal::HwInterface* DDevice::getNewConnection()
{
	if (!m_uri.empty()) {
		m_uuid = boost::uuids::random_generator()();
		createConnectionFile();
		createAddressFile();
	}

	std::cout << "loaded " << m_connectionFile << std::endl;
	try {
		uhal::ConnectionManager manager(std::string("file://") + m_connectionFile);
		uhal::HwInterface* ret = new uhal::HwInterface(manager.getDevice(m_deviceId));

		removeFiles();
		return ret;
	} catch (const std::exception& e) {
		removeFiles();
		throw e;
	}
}

void DDevice::setRefreshPeriod(DDataItem* pdi)
{
	unsigned int minimum = pdi->getRefreshPeriod();
	BOOST_FOREACH(DDataItem* di, dataitems())
		if (di->getNodeId() == pdi->getNodeId() && !di->isWritingDirection()
			&& di->getRefreshPeriod() && (!minimum || minimum > di->getRefreshPeriod()))
			minimum = di->getRefreshPeriod();
	m_monitor->setRefreshPeriod(pdi->getNodeId(), minimum);
}

void DDevice::run()
{
	std::vector<int> amounts(m_itemsCount);
	std::vector<const Monitor::Dentry*> data(m_itemsCount);

	while (m_ongoing) {
		read(amounts, data);

		int i = 0;
		unsigned int temp = data[i][amounts[i] - 1].val;
		BOOST_FOREACH(DDataItem* di, dataitems())
			if (di->isSubscribed()) {
				if (amounts[i] > 0)
					di->setReadValue(data[i][amounts[i] - 1].val, data[i][amounts[i] - 1].valid ? OpcUa_Good : OpcUa_Bad);
				i++;
			}

		endRead(amounts);
	}
}

void DDevice::notifyInvalidation(int idx)
{
	if ((int)m_opcUaIdx.size() <= idx)
		return;

	LOG(Log::WRN) << dataitems()[m_opcUaIdx[idx]]->getFullName() << " has been invalidated";
	dataitems()[m_opcUaIdx[idx]]->unsubscribe();
	m_opcUaIdx.erase(m_opcUaIdx.begin() + idx);
}

void DDevice::createConnectionFile()
{
	std::ostringstream oss;
	oss << "/tmp/connections-" << m_uuid << ".xml";
	std::ofstream f(oss.str());

	f << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
	f << "<connections>" << std::endl;
	f << "\t<connection id=\"" << m_deviceId << "\" uri=\"" << m_uri << "\" ";
	f << "address_table=\"file://address-" << m_uuid << ".xml\" />" << std::endl;
	f << "</connections>" << std::endl;

	f.close();
	m_connectionFile = oss.str();
}

void DDevice::createAddressFile()
{
	std::map< std::string, std::pair<std::string, std::string> > regs;
	BOOST_FOREACH(DDataItem* di, dataitems())
		if (regs[di->getNodeId()].first.empty())
			regs[di->getNodeId()] = std::make_pair(di->getAddress(), di->getFullName());

	std::ostringstream oss;
	oss << "/tmp/address-" << m_uuid << ".xml";
	std::ofstream f(oss.str());

	f << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
	f << "<node>" << std::endl;

	boost::regex ex("0[xX][0-9a-fA-F]+");
	for (std::map< std::string, std::pair<std::string, std::string> >::iterator it = regs.begin(); it != regs.end(); ++it) {
		if (!boost::regex_match(it->second.first.c_str(), ex))
			throw std::runtime_error("invalid hex format of address for " + getFullName() + ":" + it->second.second + " DataItem register");
		f << "\t<node id=\"" << it->first << "\" address=\"" << it->second.first << "\" />" << std::endl;
	}

	f << "</node>" << std::endl;
	f.close();
	m_addressFile = oss.str();
}

void DDevice::removeFiles()
{
	if (!m_uri.empty()) {
		std::remove(m_connectionFile.c_str());
		std::remove(m_addressFile.c_str());
	}
}


}

