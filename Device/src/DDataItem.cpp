/**
 * DDataItem.cpp
 *
 * Created on: July 16, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

#include <pyembed.h>
#include <externalmonitor.h>
#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDataItem.h>
#include <ASDataItem.h>
#include <LogIt.h>


#include <monitor.hpp>
#include <converter.hpp>

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111

BOOST_PYTHON_MODULE(exmon)
{
	ExternalMonitor::initPythonModule();
}


// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDataItem::DDataItem (const Configuration::DataItem & config

                      , DDevice * parent

                     ):
    Base_DDataItem( config

                    , parent

                  )
/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
	m_nodeId = std::string(config.nodeId().c_str());
	m_type = std::string(config.type().c_str());
	m_writingDirection = config.writingDirection();
	m_refreshPeriod = config.refreshPeriod();
	m_address = std::string(config.address().c_str());

	m_inScript = std::string(config.incomingScript().c_str());
	m_outScript = std::string(config.outgoingScript().c_str());

	m_inFuncName = std::string(config.incomingFuncName().c_str());
	m_outFuncName = std::string(config.outgoingFuncName().c_str());

	m_inFunc = m_outFunc = NULL;
	m_monitor = NULL;
	m_subscribed = true;
}

/* sample dtr */
DDataItem::~DDataItem ()
{
	delete m_inFunc;
	delete m_outFunc;

#ifdef SAMPLERATECOUNTER_H
	counter->stop();
	delete counter;
#endif
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

UaStatus DDataItem::writeRefreshPeriod ( const OpcUa_UInt32 & v)
{
	m_refreshPeriod = v;
	getParent()->setRefreshPeriod(this);
    return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DDataItem::writeWvalue ( const UaVariant & v)
{
	if (m_writingDirection) {
		// pass value through converter
		Converter::type t = Converter::getType(m_type);
		std::pair<unsigned char*, int> data = Converter::convert(v, t);
		unsigned int temp = 0;
		Converter::convert(data.first, data.second,
				(unsigned char*)&temp, sizeof(unsigned int),
				t < Converter::tint8be, true);

		// pass value either directly to the device
		if (!m_outFunc) {
			try {
				m_monitor->write(m_nodeId, temp);
				LOG(Log::DBG) << "written value " << temp << " (without outgoing script) to " << getFullName();
			} catch (const std::exception& e) {
				LOG(Log::ERR) << e.what();
				return OpcUa_Bad;
			}
		} else {
			// or through the python script first
			m_emonitor->setValue(temp);
			PyGilGuard gilGuard;
			try {
				(*m_outFunc)(m_emonitor);
			} catch (const boost::python::error_already_set&) {
				PyEmbed::logError();
				return OpcUa_Bad;
			}
		}
		return OpcUa_Good;
	}

	return OpcUa_BadUserAccessDenied;
}


// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DDataItem::setMonitor(Monitor* monitor)
{
	const std::string fullName(getParent()->getFullName() + ":" + getFullName());
	if (m_nodeId.empty())
		throw std::runtime_error("NodeId empty in " + fullName);

	// initialize monitor
	m_monitor = monitor;
	m_emonitor = boost::shared_ptr<ExternalMonitor>(new ExternalMonitor(0, monitor, m_nodeId, getFullName()));
	if (!m_writingDirection && (!m_monitor->getRefreshPeriod(m_nodeId) || m_monitor->getRefreshPeriod(m_nodeId) > m_refreshPeriod))
		m_monitor->setRefreshPeriod(m_nodeId, m_refreshPeriod);
	namespace python = boost::python;

	PyGilGuard gilGuard;
	initexmon();

	// initialize scripts
	if (!m_inScript.empty() && !m_inFuncName.empty())
		m_inFunc = PyEmbed::getInstance()->loadScript(m_inScript, m_inFuncName, 1, "incoming", fullName);

	if (!m_outScript.empty() && !m_outFuncName.empty())
		m_outFunc = PyEmbed::getInstance()->loadScript(m_outScript, m_outFuncName, 1, "outgoing", fullName);

	m_addressSpaceLink->setRefreshPeriod(m_refreshPeriod, OpcUa_Good, UaDateTime::now());

#ifdef SAMPLERATECOUNTER_H
	counter = new SampleRateCounter(1000, fullName, std::cout);
	counter->start();
#endif
}

const std::string& DDataItem::getNodeId()
{
	return m_nodeId;
}

const std::string& DDataItem::getAddress()
{
	return m_address;
}

unsigned int DDataItem::getRefreshPeriod()
{
	return m_refreshPeriod;
}

bool DDataItem::isWritingDirection()
{
	return m_writingDirection;
}

std::string DDataItem::getType()
{
	return m_type;
}

void DDataItem::setReadValue(unsigned int x, OpcUa_StatusCode scode)
{
	// pass value through script, if available
	if (m_inFunc) {
		PyGilGuard gilGuard;
		try {
			x = boost::python::extract<unsigned int>((*m_inFunc)(x));
		} catch (const boost::python::error_already_set&) {
			PyEmbed::logError();
			m_addressSpaceLink->setRvalue(0, OpcUa_Bad, UaDateTime::now());
			return;
		}
	}

	// send it to the address space
	UaVariant temp;
	Converter::convert((const unsigned char*)&x, temp, Converter::getType(m_type));
	m_addressSpaceLink->setRvalue(temp, scode, UaDateTime::now());

#ifdef SAMPLERATECOUNTER_H
	counter->increment();
#endif
}

void DDataItem::unsubscribe()
{
	m_subscribed = false;
	m_addressSpaceLink->setRvalue(0, OpcUa_Bad, UaDateTime::now());
}

bool DDataItem::isSubscribed()
{
	return m_subscribed;
}



}

