/*
 * monitor.cpp
 *
 * Created on: July 6, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

#include <monitor.hpp>

#include <generalbuffer.hpp>
#include <DDevice.h>
#include <LogIt.h>

using namespace uhal;
using namespace std;

namespace bc = boost::chrono;
typedef boost::chrono::high_resolution_clock bch;

Monitor::Monitor(HwInterface* hwInterface, const std::string &fullName, unsigned int buffersize)
	: m_hwInterface(hwInterface)
	, m_buffersSize(buffersize),
	m_fullName(fullName)
{
	m_delay = 10;
	m_enabled = true;
	m_upolicy = NewValue;
	m_device = NULL;
	m_timeout = 100000;
}

Monitor::~Monitor()
{

}

void Monitor::setInterval(int delay)
{
	m_delay = delay;
}

void Monitor::setRefreshPeriod(const string& name, unsigned int interval)
{
	m_nodesMap[name]->interval = interval;
}

unsigned int Monitor::getRefreshPeriod(const string& name)
{
	if (m_nodesMap.find(name) == m_nodesMap.end())
		return 0;
	return m_nodesMap[name]->interval;
}

void Monitor::setUpdatePolicy(UpdatePolicy upolicy)
{
	m_upolicy = upolicy;
}

void Monitor::setDevice(Device::DDevice* device)
{
	m_device = device;
}

void Monitor::setTimeout(unsigned int t)
{
	m_timeout = t;
	m_hwInterface->setTimeoutPeriod(t);
}

void Monitor::start()
{
	if (m_thr.joinable())
		return;

	m_ongoing = true;
	setEnabled(true);
	cout << "start" << endl;
	m_thr = boost::thread(&Monitor::run, this);
}

void Monitor::run()
{
	while (m_ongoing) {
		m_localMutex.lock();
		bch::time_point t = bch::now();

		// create requests for needed registers
		bool readRequest = false;
		for (unsigned int i = 0; i < m_nodes.size(); i++)
			if (m_nodes[i]->interval && m_nodes[i]->isUpdateDue(t))
			{
				m_nodes[i]->resp = m_nodes[i]->node->read();
				LOG(Log::TRC) << "Just read node: " << m_nodes[i]->name << ", raw value was: " << m_nodes[i]->resp;
				readRequest = true;
			}

		try {
			// dispatch the request
			if (readRequest)
				m_hwInterface->dispatch();
		} catch (...) {
			// in case of failure, rebuild the hwInterface object
			delete m_hwInterface;
			m_hwInterface = m_device->getNewConnection();
			m_hwInterface->setTimeoutPeriod(m_timeout);

			// get references to its nodes
			for (unsigned int i = 0; i < m_nodes.size(); i++)
				m_nodes[i]->node = &m_hwInterface->getNode(m_nodes[i]->name);

			// set values with invalid status to update the address space to OpcUaBad
			for (unsigned int i = 0; i < m_nodes.size(); i++)
				m_nodes[i]->add(Dentry(0, false));
			m_localMutex.unlock();

			LOG(Log::WRN) << "connection has been lost, attempting new connection ...";
			boost::this_thread::sleep_for(boost::chrono::seconds(1));
			continue;
		}

		// iterate through nodes and add them to buffers
		for (unsigned int i = 0; i < m_nodes.size(); i++)
			if (m_nodes[i]->interval) {
				if (m_nodes[i]->resp.valid()) {
					if (m_nodes[i]->isUpdateDue(t, false) && (m_upolicy == Always || m_nodes[i]->isNewValue()))
						m_nodes[i]->add(Dentry(m_nodes[i]->resp.value(), t, m_nodes[i]->resp.valid()));
				} else {
					LOG(Log::WRN) << "In monitor of device: " << m_fullName << ", monitored register " << m_nodes[i]->name << " is reported invalid (perhaps firmware failure or wrong register address)";
				}
			}

		m_localMutex.unlock();
		boost::this_thread::sleep_for(boost::chrono::milliseconds(m_delay));
	}
}

void Monitor::setEnabled(bool ok)
{
	boost::lock_guard<boost::mutex> guard(m_mutex);
	m_enabled = ok;
	m_cond.notify_all();
}

void Monitor::stop()
{
	if (!m_thr.joinable())
		return;
	setEnabled(false);

	m_ongoing = false;
	m_thr.join();
}

Monitor::MonitoredNode::MonitoredNode(const uhal::Node& node, int bufsize, boost::mutex& mutex, boost::condition_variable& cond, bool& enabled)
	: node(&node)
	, name(node.getId())
	, interval(0)
	, buff(new GeneralBuffer<Dentry>(bufsize, mutex, cond, enabled))
	, firstValue(true)
	, update(true)
	, oldValue(0)
	, oldTimeStamp(bch::now()) {
	cout << "created " << name << endl;
}

Monitor::MonitoredNode::~MonitoredNode()
{
	delete buff;
}

void Monitor::MonitoredNode::add(const Dentry& de)
{
	oldValue = de.val;
	oldTimeStamp = de.t;
	firstValue = update = false;

	buff->beginWrite();
	buff->add(de);
	buff->endWrite();
}

bool Monitor::MonitoredNode::isUpdateDue(bch::time_point t, bool keep)
{
	if (update) {
		update = keep;
		return true;
	}
	if (bc::duration_cast<bc::milliseconds>(bch::now() - oldTimeStamp).count() > interval) {
		update = keep;
		oldTimeStamp = t;
		return true;
	}
	return false;
}

bool Monitor::MonitoredNode::isNewValue()
{
	return oldValue != resp.value() || firstValue;
}

Monitor::Dentry::Dentry()
	: val(0)
	, valid(false) {}

Monitor::Dentry::Dentry(uint32_t val, bool valid)
	: val(val)
	, valid(valid)
{
	t = bch::now();
}

Monitor::Dentry::Dentry(uint32_t val, bch::time_point t, bool valid)
	: val(val)
	, t(t)
	, valid(valid) {}

void Monitor::subscribe(BufferReader<Dentry>* br, const string& name)
{
	// set control mutex and conditions to the subscribing buffer reader
	if (!static_cast<BRAccessInterface<Dentry>*>(br)->isControlInitialized())
		static_cast<BRAccessInterface<Dentry>*>(br)->setControl(&m_mutex, &m_cond, &m_enabled);

	// get the buffer for the requested register and
	GeneralBuffer<Dentry>* buffer = getNodeBuffer(name);
	buffer->requestAccessId(br);
}

uint32_t Monitor::read(const string& name)
{
	boost::lock_guard<boost::mutex> guard(m_localMutex);
	uhal::ValWord<uint32_t> val = m_hwInterface->getNode(name).read();
	m_hwInterface->dispatch();
	return val.value();
}

void Monitor::write(const string& name, uint32_t val)
{
	boost::lock_guard<boost::mutex> guard(m_localMutex);
	m_hwInterface->getNode(name).write(val);
	m_hwInterface->dispatch();
}

GeneralBuffer<Monitor::Dentry>* Monitor::getNodeBuffer(const string& name)
{
	// search if register is already in the list and has a buffer
	boost::lock_guard<boost::mutex> guard(m_localMutex);
	map<string, MonitoredNode*>::iterator it = m_nodesMap.find(name);
	if (it != m_nodesMap.end())
		return it->second->buff;

	// create monitor node and buffer for the register, add it to the map
	m_nodes.push_back(new MonitoredNode(m_hwInterface->getNode(name), m_buffersSize, m_mutex, m_cond, m_enabled));
	m_nodesMap[name] = m_nodes.back();
	return m_nodes.back()->buff;
}

