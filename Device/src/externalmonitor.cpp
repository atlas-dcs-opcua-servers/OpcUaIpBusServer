/*
 * externalmonitor.cpp
 *
 * Created on: August, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

#include <pyembed.h>
#include <LogIt.h>

#include <externalmonitor.h>
#include <monitor.hpp>
#include <string>
#include <DDataScript.h>
#include <boost/foreach.hpp>

namespace Device
{

ExternalMonitor::ExternalMonitor(DDataScript *dataScript, Monitor* monitor, const std::string& nodeId, const std::string& fullName)
		: m_monitor(monitor)
		, m_nodeId(nodeId)
		, m_value(0)
		, ongoing(true),
		m_dataScript(dataScript),
		m_fullName(fullName)
{}

unsigned int ExternalMonitor::read()
{
	PyThreadGuard guard;
	if (m_nodeId.empty())
		throw std::runtime_error("no own node id set");
	m_value = m_monitor->read(m_nodeId);
	return m_value;
}

unsigned int ExternalMonitor::readReg(const std::string& nodeId)
{
	PyThreadGuard guard;
	return m_monitor->read(nodeId);
}

void ExternalMonitor::write(unsigned int x)
{
	PyThreadGuard guard;
	if (m_nodeId.empty())
		throw std::runtime_error("no own node id set");
	m_monitor->write(m_nodeId, x);
	LOG(Log::DBG) << "written value " << x << " (using outgoing script) to " << m_fullName;
	m_value = x;
}

void ExternalMonitor::writeReg(const std::string& nodeId, unsigned int x)
{
	PyThreadGuard guard;
	m_monitor->write(nodeId, x);
}

unsigned int ExternalMonitor::getValue()
{
	return m_value;
}

void ExternalMonitor::setValue(unsigned int value)
{
	m_value = value;
}

bool ExternalMonitor::getOngoing()
{
	return ongoing;
}

void ExternalMonitor::stopOngoing()
{
	ongoing = false;
}



void ExternalMonitor::initPythonModule()
{
	using namespace boost::python;
	static bool ok = false;
	if (ok)
		return;
	ok = true;

	class_<ExternalMonitor, boost::shared_ptr<ExternalMonitor> >("ExternalMonitor")
		.def("write", &ExternalMonitor::write)
		.def("writeReg", &ExternalMonitor::writeReg)
		.def("read", &ExternalMonitor::read)
		.def("readReg", &ExternalMonitor::readReg)
		.def("release", &ExternalMonitor::releaseGil)
		.def("storeOutput", &ExternalMonitor::storeOutputVariable)
		.def("getOutputVariables", &ExternalMonitor::getOutputVariables)
		.add_property("value", &ExternalMonitor::getValue)
		.add_property("ongoing", &ExternalMonitor::getOngoing);
}

void ExternalMonitor::releaseGil()
{
	PyThreadGuard guard;
}

void ExternalMonitor::storeOutputVariable (const std::string &name, unsigned int value)
{
	m_dataScript->storeOutputVariable( name, value );
}

boost::python::list ExternalMonitor::getOutputVariables () const
{
	boost::python::list ret;
	BOOST_FOREACH( const std::string &s,  m_dataScript->getOutputVariables() )
	{
		ret.append (boost::python::str(s));
	}

	return ret;
}

}
