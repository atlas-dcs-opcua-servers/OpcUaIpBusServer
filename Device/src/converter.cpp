/**
 * converter.cpp
 *
 * Created on: July 20, 215
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 *
 */

#include <converter.hpp>

#include <iostream>
#include <stdexcept>
using namespace std;

bool Converter::machineLittleEndian;
bool Converter::initialized;

void Converter::init()
{
	unsigned int x = 1;
	unsigned char* px = (unsigned char*)&x;
	machineLittleEndian = px[0] == 1;
	initialized = true;
}

const unsigned char* Converter::convert(const unsigned char* in, int inSize, unsigned char* out, int outSize, bool deviceLittleEndian, bool to)
{
	if (!initialized)
		init();

	if (!to) {
		bool man = machineLittleEndian;
		machineLittleEndian = deviceLittleEndian;
		deviceLittleEndian = man;
	}

	int size = std::min(inSize, outSize);
	if (deviceLittleEndian == machineLittleEndian)
		if (machineLittleEndian)
			for (int i = 0; i < size; i++)
				out[i] = in[i];
		else
			for (int i = 0; i < size; i++)
				out[outSize - 1 - i] = in[inSize - 1 - i];
	else
		if (machineLittleEndian)
			for (int i = 0; i < size; i++)
				out[outSize - 1 - i] = in[i];
		else
			for (int i = 0; i < size; i++)
				out[i] = in[inSize - 1 - i];

	if (inSize <= outSize)
		return NULL;
	return in + size;
}

Converter::type Converter::getType(const std::string& t)
{
	if (t == "int8le") return tint8le;
	if (t == "uint8le") return tuint8le;
	if (t == "int16le") return tint16le;
	if (t == "uint16le") return tuint16le;
	if (t == "int32le") return tint32le;
	if (t == "uint32le") return tuint32le;
	if (t == "int64le") return tint64le;
	if (t == "uint64le") return tuint64le;
	if (t == "floatle") return tfloatle;

	if (t == "int8be") return tint8be;
	if (t == "uint8be") return tuint8be;
	if (t == "int16be") return tint16be;
	if (t == "uint16be") return tuint16be;
	if (t == "int32be") return tint32be;
	if (t == "uint32be") return tuint32be;
	if (t == "int64be") return tint64be;
	if (t == "uint64be") return tuint64be;
	if (t == "floatbe") return tfloatbe;
	throw runtime_error("Invalid data type provided");
}

std::pair<unsigned char*, int> Converter::convert(const UaVariant& variant, type t)
{
	switch (t) {
	case tint8le:
	case tint8be: {
		OpcUa_SByte* x = new OpcUa_SByte();
		variant.toSByte(*x);
		return make_pair<unsigned char*, int>((unsigned char*)x, 1);
	}

	case tuint8le:
	case tuint8be: {
		OpcUa_Byte* x = new OpcUa_Byte();
		variant.toByte(*x);
		return make_pair<unsigned char*, int>((unsigned char*)x, 1);
	}

	case tint16le:
	case tint16be: {
		OpcUa_Int16* x = new OpcUa_Int16();
		variant.toInt16(*x);
		return make_pair<unsigned char*, int>((unsigned char*)x, 2);
	}

	case tuint16le:
	case tuint16be: {
		OpcUa_UInt16* x = new OpcUa_UInt16();
		variant.toUInt16(*x);
		return make_pair<unsigned char*, int>((unsigned char*)x, 2);
	}

	case tint32le:
	case tint32be: {
		OpcUa_Int32* x = new OpcUa_Int32();
		variant.toInt32(*x);
		return make_pair<unsigned char*, int>((unsigned char*)x, 4);
	}

	case tuint32le:
	case tuint32be: {
		OpcUa_UInt32* x = new OpcUa_UInt32();
		variant.toUInt32(*x);
		return make_pair<unsigned char*, int>((unsigned char*)x, 4);
	}

	case tfloatle:
	case tfloatbe: {
		OpcUa_Float* x = new OpcUa_Float();
		variant.toFloat(*x);
		return make_pair<unsigned char*, int>((unsigned char*)x, 4);
	}

	default:
		return make_pair<unsigned char*, int>(NULL, 0);
	}
}

const unsigned char* Converter::convert(const unsigned char* src, UaVariant& value, type t)
{
	const unsigned char* ret;
	bool deviceLittleEndian = t < tint8be;

	switch (t) {
	case tint8be:
	case tint8le: {
		OpcUa_SByte x = 0;
		ret = convert(src, sizeof(OpcUa_UInt32), (unsigned char*)&x, sizeof(OpcUa_SByte), deviceLittleEndian);
		value.setSByte(x);
		return ret;
	}

	case tuint8be:
	case tuint8le: {
		OpcUa_Byte x = 0;
		ret = convert(src, sizeof(OpcUa_UInt32), (unsigned char*)&x, sizeof(OpcUa_Byte), deviceLittleEndian);
		value.setByte(x);
		return ret;
	}

	case tint16be:
	case tint16le: {
		OpcUa_Int16 x = 0;
		ret = convert(src, sizeof(OpcUa_UInt32), (unsigned char*)&x, sizeof(OpcUa_Int16), deviceLittleEndian);
		value.setInt16(x);
		return ret;
	}

	case tuint16be:
	case tuint16le: {
		OpcUa_UInt16 x = 0;
		ret = convert(src, sizeof(OpcUa_UInt32), (unsigned char*)&x, sizeof(OpcUa_UInt16), deviceLittleEndian);
		value.setUInt16(x);
		return ret;
	}

	case tint32be:
	case tint32le: {
		OpcUa_Int32 x = 0;
		ret = convert(src, sizeof(OpcUa_UInt32), (unsigned char*)&x, sizeof(OpcUa_Int32), deviceLittleEndian);
		value.setInt32(x);
		return ret;
	}

	case tuint32be:
	case tuint32le: {
		unsigned int x = 0;
		ret = convert(src, sizeof(OpcUa_UInt32), (unsigned char*)&x, sizeof(OpcUa_UInt32), deviceLittleEndian);
		value.setUInt32(x);
		return ret;
	}

	case tint64be:
	case tint64le: {
		OpcUa_Int32 x = 0;
		ret = convert(src, sizeof(OpcUa_UInt64), (unsigned char*)&x, sizeof(OpcUa_Int64), deviceLittleEndian);
		value.setInt64(x);
		return ret;
	}

	case tuint64be:
	case tuint64le: {
		OpcUa_UInt64 x = 0;
		ret = convert(src, sizeof(OpcUa_UInt64), (unsigned char*)&x, sizeof(OpcUa_UInt64), deviceLittleEndian);
		value.setUInt64(x);
		return ret;
	}

	case tfloatbe:
	case tfloatle: {
		OpcUa_Float x = 0;
		ret = convert(src, sizeof(OpcUa_UInt32), (unsigned char*)&x, sizeof(OpcUa_Float), deviceLittleEndian);
		value.setFloat(x);
		return ret;
	}
	}
}
