#include <sampleratecounter.h>

boost::mutex SampleRateCounter::streamMutex;

SampleRateCounter::SampleRateCounter(unsigned int interval, const std::string& name, std::ostream& out)
	: m_interval(interval)
	, m_name(name)
	, m_out(out)
{
	m_count = 0;
	m_ongoing = false;
}

SampleRateCounter::~SampleRateCounter()
{

}

void SampleRateCounter::increment()
{
	m_count++;
}

void SampleRateCounter::setInterval(unsigned int interval)
{
	this->m_interval = interval;
}

void SampleRateCounter::start()
{
	if (m_ongoing)
		return;
	m_ongoing = true;
	m_thr = boost::thread(&SampleRateCounter::run, this);
}

void SampleRateCounter::run()
{
	while (m_ongoing) {
		float freq = m_count * 1000. / m_interval;

		m_streamMutex.lock();
		m_out << m_name << ": " << freq << " samples / sec" << std::endl;
		m_streamMutex.unlock();

		m_count = 0;
		boost::this_thread::sleep_for(boost::chrono::milliseconds(m_interval));
	}
}

void SampleRateCounter::stop()
{
	if (!m_ongoing)
		return;
	m_ongoing = false;
	if (m_thr.joinable())
		m_thr.join();
}
