
/*
 * DataScript.cpp
 *
 * Created on: August, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

#include <pyembed.h>
#include <externalmonitor.h>

#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDataScript.h>
#include <ASDataScript.h>

#include <libgen.h>
#include <string.h>
#include <stdexcept>
#include <iostream>
#include <LogIt.h>

#include <DDataScriptVariable.h>

namespace Device
{

// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111

BOOST_PYTHON_MODULE(emonitor)
{
	ExternalMonitor::initPythonModule();
}




// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDataScript::DDataScript (const Configuration::DataScript & config

                          , DDevice * parent

                         ):
    Base_DDataScript( config

                      , parent

                    )
/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
	m_path = std::string(config.path().c_str());
	m_funcName = std::string(config.funcName().c_str());
	m_monitor = NULL;
}

/* sample dtr */
DDataScript::~DDataScript ()
{
}

/* delegators for cachevariables and externalvariables */



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DDataScript::init(Monitor* monitor)
{
	namespace python = boost::python;
	PyGilGuard gilGuard;
	initemonitor();

	const std::string fullName(getParent()->getFullName() + ":" + getFullName());
	m_func = PyEmbed::getInstance()->loadScript(m_path, m_funcName, 1, "independnet", fullName);
	m_monitor = monitor;

	m_emonitor = boost::shared_ptr<ExternalMonitor>(new ExternalMonitor(this, monitor, "", getFullName()));
	m_thr = boost::thread(&DDataScript::run, this);
}

void DDataScript::destroy()
{
	m_emonitor->stopOngoing();
	m_thr.join();
	delete m_func;
}

void DDataScript::run()
{
	PyGilGuard gilGuard;
	try {
		(*m_func)(m_emonitor);
	} catch (const boost::python::error_already_set&)
	{
		std::string error = PyEmbed::logError();
		getAddressSpaceLink()->setLastPythonExceptionAsText(error.c_str(), OpcUa_Good);
	}
}

void DDataScript::storeOutputVariable( std::string name, unsigned int value )
{
	BOOST_FOREACH(DDataScriptVariable *variable, datascriptvariables())
	{
		if (variable->getName() == name)
		{
			// ok found it
			variable->update(value);
		}

	}
}

std::vector<std::string> DDataScript::getOutputVariables () const
{
	std::vector<std::string> output;
	BOOST_FOREACH(DDataScriptVariable *variable, datascriptvariables())
	{
		output.push_back(variable->getName());
	}
	return output;
}



}


