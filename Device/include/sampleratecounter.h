#ifndef SAMPLERATECOUNTER_H
#define SAMPLERATECOUNTER_H

#include <ostream>
#include <sstream>
#include <boost/thread.hpp>
#include <boost/atomic.hpp>

class SampleRateCounter
{
public:
	SampleRateCounter(unsigned int interval, const std::string& name, std::ostream& out);
	~SampleRateCounter();

	SampleRateCounter(const SampleRateCounter&) = delete;
	SampleRateCounter& operator=(const SampleRateCounter&) = delete;

	void setInterval(unsigned int interval);
	void increment();

	void start();
	void stop();

private:
	void run();

	unsigned int m_interval;
	boost::atomic<unsigned int> m_count;
	const std::string m_name;
	std::ostream& m_out;

	boost::thread m_thr;
	static boost::mutex m_streamMutex;
	bool m_ongoing;
};

#endif
