/**
 * converter.hpp
 *
 * Created on: July 20, 215
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 *
 */

#ifndef CONVERTER_HPP
#define CONVERTER_HPP

#include <uavariant.h>
#include <map>
#include <string>
#include <utility>

/**
 * Converter class
 */
class Converter
{
public:
	/**
	 * Conversion data types enums.
	 * format: t type endianness
	 * e.g.: tuint32le -> unsigned integer of 32 bits, little endian
	 */
	enum type {
		tint8le, tuint8le, tint16le, tuint16le, tint32le, tuint32le, tint64le, tuint64le, tfloatle,
		tint8be, tuint8be, tint16be, tuint16be, tint32be, tuint32be, tint64be, tuint64be, tfloatbe
	};

	/**
	 * Method which gets a string description of the desired type and returns an enum.
	 * @param t string of the type
	 * @return enum type returned
	 */
	static type getType(const std::string& t);
	/**
	 * Method which takes a byte array src, a destination variable value and its type t.
	 * @param src source byte array
	 * @param value destination UaVariant variable
	 * @param t type of the conversion
	 * @return shifted pointer of src after the conversion has read sizeof(t) bytes
	 */
	static const unsigned char* convert(const unsigned char* src, UaVariant& value, type t);
	static std::pair<unsigned char*, int> convert(const UaVariant& variant, type t);

	/**
	 * Conversion method
	 * @param in source byte array
	 * @param inSize length of source byte array
	 * @param out destination byte array
	 * @param outSize length of destination byte array
	 * @param deviceLittleEndian true if the desired conversion should be to little endian
	 * @return shifted pointer of in after the conversion has read outSize bytes
	 */
	static const unsigned char* convert(const unsigned char* in, int inSize, unsigned char* out, int outSize, bool deviceLittleEndian, bool to = false);

private:
	/**
	 * Initialization method that determines the endianness of the platform.
	 */
	static void init();

	static bool machineLittleEndian, initialized;

};

#endif
