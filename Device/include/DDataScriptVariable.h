
/* This is device header stub */


#ifndef __DDataScriptVariable__H__
#define __DDataScriptVariable__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DDataScriptVariable.h>


namespace Device
{




class
    DDataScriptVariable
    : public Base_DDataScriptVariable
{

public:
    /* sample constructor */
    explicit DDataScriptVariable ( const Configuration::DataScriptVariable & config

                                   , DDataScript * parent

                                 ) ;
    /* sample dtr */
    ~DDataScriptVariable ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DDataScriptVariable( const DDataScriptVariable & ) = delete;
    DDataScriptVariable& operator=(const DDataScriptVariable &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    std::string getName() { return m_name; }
    void update (unsigned int rawValue);

private:
    std::string m_name;


};





}

#endif // include guard
