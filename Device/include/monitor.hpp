/*
 * monitor.h
 *
 * Created on: July 6, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

#ifndef MONITOR_H
#define MONITOR_H

#include <vector>
#include <map>
#include <string>
#include <sys/time.h>
#include <uhal/uhal.hpp>

#include <boost/thread.hpp>
#include <boost/atomic.hpp>
#include <boost/chrono.hpp>

template <typename T> class GeneralBuffer;
template <typename T> class BufferReader;

namespace Device {
class DDevice;
}

/**
 * Monitor class used for monitoring data.
 */
class Monitor
{
public:
	/**
	 * data entry structure recorded by the monitor
	 */
	struct Dentry {
		Dentry();
		Dentry(uint32_t val, bool valid = true);
		Dentry(uint32_t val, boost::chrono::high_resolution_clock::time_point t, bool valid = true);

		/**
		 * actual value
		 */
		uint32_t val;
		/**
		 * timestamp
		 */
		boost::chrono::high_resolution_clock::time_point t;

		bool valid;
	};

	/**
	 * Update policy enum
	 */
	enum UpdatePolicy {
		NewValue,
		Always
	};

	/**
	 * Constructor which receives a pointer to a created HwInterface class.
	 * @param fullName is name of the device it handles, required only for logging
	 */
	Monitor(uhal::HwInterface* hwInterface, const std::string &fullName, unsigned int bufsize = 10);
	~Monitor();

	Monitor(const Monitor&) = delete;
	Monitor& operator=(const Monitor&) = delete;

	/**
	 * Sets the interval for polling data from the device.
	 * @param delay polling interval in milliseconds
	 */
	void setInterval(int delay);
	/**
	 * Sets the interval for polling a specific register
	 * @param name name of the register
	 * @param interval time interval in milliseconds
	 */
	void setRefreshPeriod(const std::string& name, unsigned int interval);

	unsigned int getRefreshPeriod(const std::string& name);

	/**
	 * Sets the data update policy.
	 * @param uplolicy the new policy
	 */
	void setUpdatePolicy(UpdatePolicy upolicy);

	void setDevice(Device::DDevice* device);

	void setTimeout(unsigned int t);

	/**
	 * Starts the monitoring thread.
	 */
	void start();
	/**
	 * Stops the monitoring thread.
	 */
	void stop();

	/**
	 * Subscribe method called by a buffer reader, requesting a subscription
	 * to a desired register
	 * @param br pointer to the buffer reader
	 * @param name name of the desired register
	 */
	void subscribe(BufferReader<Dentry>* br, const std::string& name);
	/**
	 * Method to write a value to a register. The method takes care
	 * of synchronization with the main thread.
	 * @param name name of the register
	 * @param val value to be written
	 */
	void write(const std::string& name, uint32_t val);
	uint32_t read(const std::string& name);

private:
	/**
	 * MonitoredNode structure
	 * structure which incapsulates details about a monitored node/register.
	 */
	struct MonitoredNode
	{
		/**
		 * Constructor
		 * @param node reference to a uhal Node belonging to the HwInterface
		 * @param mutex reference to a mutex, used for synchronization between readers and writer
		 * @param cond conditional variable, used for synchronization between readers and writer
		 * @param enabled boolean reference which specifies if the monitor is active or not
		 */
		MonitoredNode(const uhal::Node& node, int bufsize, boost::mutex& mutex, boost::condition_variable& cond, bool& enabled);
		~MonitoredNode();

		/**
		 * Method to add a data entry to the buffer.
		 * @param de data entry
		 */
		void add(const Dentry& de);
		/**
		 * Checks if more time than the refresh period has passed since the last update
		 */
		bool isUpdateDue(boost::chrono::high_resolution_clock::time_point t, bool keep = true);
		/**
		 * Checks if the new value is different from the current one
		 */
		bool isNewValue();

		const uhal::Node* node;
		const std::string name;
		unsigned int interval;
		GeneralBuffer<Dentry>* buff;

		bool firstValue, update;
		uint32_t oldValue;
		boost::chrono::high_resolution_clock::time_point oldTimeStamp;
		uhal::ValWord<uint32_t> resp;
	};

	/**
	 * Method to be ran on a parallel thread
	 * @param param parameter received by it, pointer to the calling class
	 */
	void run();
	/**
	 * Method which sets the enabled state of the monitoring class.
	 * @param ok boolean value for the state.
	 */
	void setEnabled(bool ok);
	/**
	 * Method to return a pointer to the buffer responsible for the data of a register.
	 * @param name name of the register
	 * @return pointer to the buffer
	 */
	GeneralBuffer<Dentry>* getNodeBuffer(const std::string& name);

	boost::thread m_thr;
	boost::atomic<bool> m_ongoing;
	int m_delay;
	UpdatePolicy m_upolicy;

	boost::mutex m_mutex;
	boost::condition_variable m_cond;
	bool m_enabled;

	//TODO: rename localMutex to IpBus access mutex ot so
	boost::mutex m_localMutex;

	uhal::HwInterface* m_hwInterface;
	unsigned int m_timeout, m_buffersSize;
	Device::DDevice* m_device;

	std::vector<MonitoredNode*> m_nodes;
	std::map<std::string, MonitoredNode*> m_nodesMap;

	//! Used only for logging
	std::string m_fullName;
};

#endif
