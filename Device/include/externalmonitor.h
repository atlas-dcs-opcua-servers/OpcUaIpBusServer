/*
 * externalmonitor.h
 *
 * Created on: August, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

#ifndef EXTERNALMONITOR_H
#define EXTERNALMONITOR_H

#include <string>
#include <vector>

class Monitor;
namespace boost { namespace python { struct object; class list;} }

namespace Device
{

class DDataScript;

/**
 * Class to be used as a delegated interface to the python script.
 */
// TODO: check why we cannot delete copy constructor
class ExternalMonitor
{
public:
	/**
	 * @param monitor is required in order to provide read/write interface to IpBus data
	 * @param fullName is used only for printing debug info
	 */
	ExternalMonitor(DDataScript *dataScript = 0, Monitor* monitor = NULL, const std::string& nodeId = "", const std::string& fullName = "");
	ExternalMonitor& operator=(const ExternalMonitor&) = delete;

	/**
	 * Reads monitored register and returns its value.
	 */
	unsigned int read();
	/**
	 * Reads an arbitrary register.
	 */
	unsigned int readReg(const std::string& nodeId);

	/**
	 * Writes monitored register.
	 */
	void write(unsigned int x);
	/**
	 * Writes arbitrary register.
	 */
	void writeReg(const std::string& nodeId, unsigned int x);

	/**
	 * Reads current register value.
	 */
	unsigned int getValue();
	/**
	 * Sets current register value.
	 */
	void setValue(unsigned int value);

	/**
	 * Gets boolean flag which specifies if the server is still up.
	 */
	bool getOngoing();
	/**
	 * Sets stopping server flag.
	 */
	void stopOngoing();

	/**
	 * Python interface class initiation routine.
	 */
	static void initPythonModule();
	/**
	 * Releases GIL temporarily.
	 */
	void releaseGil();

	/**
	 * Puts the output value produced by the Python script into one of DataScript variables configured for this script.
	 * This is actually made available to Python.
	 */
	void storeOutputVariable (const std::string &name, unsigned int value);

	boost::python::list getOutputVariables () const;

private:
	Monitor* m_monitor;
	std::string m_nodeId;
	unsigned int m_value;
	bool ongoing;
	DDataScript *m_dataScript;
	//! The full name is required only for debugging
	std::string m_fullName;

};

}

#endif
