/**
 * DDataItem.h
 *
 * Created on: July 16, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */


#ifndef __DDataItem__H__
#define __DDataItem__H__

#include <vector>
#include <string>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DDataItem.h>

class Monitor;
class ExternalMonitor;
namespace boost { namespace python { struct object; } }

namespace Device
{




class
    DDataItem
    : public Base_DDataItem
{

public:
    /* sample constructor */
    explicit DDataItem ( const Configuration::DataItem & config

                         , DDevice * parent

                       ) ;
    /* sample dtr */
    ~DDataItem ();




    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeRefreshPeriod ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeWvalue ( const UaVariant & v);

private:
    /* Delete copy constructor and assignment operator */
    DDataItem( const DDataItem & ) = delete;
    DDataItem& operator=(const DDataItem &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    /**
     * Mothod used for setting the monitor class.
     * @param monitor pointer to the monitor class
     */
    void setMonitor(Monitor* monitor);
    /**
     * Method used for getting ID string of the monitored node/register
     * @return string of the node
     */
    const std::string& getNodeId();

    const std::string& getAddress();

    unsigned int getRefreshPeriod();

    bool isWritingDirection();

    /**
     * Method used for getting the data type of the monitored node/register.
     * @return string of the data type
     */
    std::string getType();
    /**
     * Method used for setting the value read by the buffer reader
     * @param x value read
     */
    void setReadValue(unsigned int x, OpcUa_StatusCode scode = OpcUa_Good);

    /**
     * Set unsubscribed flag
     */
    void unsubscribe();
    /**
     * Returns unsubscribed flag
     */
    bool isSubscribed();

private:
    Monitor* m_monitor;
    boost::shared_ptr<ExternalMonitor> m_emonitor;

    std::string m_nodeId, m_address;
    std::string m_type;
    bool m_writingDirection, m_subscribed;
    unsigned int m_refreshPeriod;

    std::string m_inScript, m_outScript;
    std::string m_inFuncName, m_outFuncName;
    boost::python::object *m_inFunc, *m_outFunc;

#ifdef SAMPLERATECOUNTER_H
    SampleRateCounter* counter;
#endif
};





}

#endif // include guard

