/**
 * DDevice.h
 * Created on: July 16, 2015
 * Author: csoare
 */


#ifndef __DDevice__H__
#define __DDevice__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DDevice.h>
#include <boost/uuid/uuid.hpp>

#include "generalbuffer.hpp"
#include "monitor.hpp"
#include "converter.hpp"


namespace Device
{


class
    DDevice
    : public Base_DDevice, public BufferReader<Monitor::Dentry>
{

public:
    /* sample constructor */
    explicit DDevice ( const Configuration::Device & config

                       , DRoot * parent

                     ) ;
    /* sample dtr */
    ~DDevice ();

private:
    /* Delete copy constructor and assignment operator */
    DDevice( const DDevice & ) = delete;
    DDevice& operator=(const DDevice &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    /**
	 * Initializes the monitoring components
	 */
	void init();
	/**
	 * Destroys the monitoring components in a controlled way.
	 */
	void destroy();


	/* delegators for
	cachevariables and sourcevariables */

	UaStatus writeBaseInterval ( const OpcUa_UInt32 & v);
	UaStatus writeTimeoutPeriod ( const OpcUa_UInt32 & v);

	uhal::HwInterface* getNewConnection();
	void setRefreshPeriod(DDataItem* di);

private:
    /**
     * Method used for continously reading the buffer; it runs on a separate thread.
     * @param param the parameter given to the thread (pointer to the class)
     */
    void run();
    /**
     * Callback for being unsubscribed from a buffer
     */
    void notifyInvalidation(int idx);
    /**
     * Creates temporary connection file based on device definitions
     */
    void createConnectionFile();
    /**
     * Creates temporary address file based on register definitions in child data items
     */
    void createAddressFile();
    /**
     * Removes temporary connection and address files
     */
    void removeFiles();

    boost::thread m_thr;
    boost::atomic<bool> m_ongoing;
    int m_itemsCount, m_buffersSize, m_baseInterval;

    std::vector<int> m_opcUaIdx;
    std::string m_connectionFile, m_addressFile, m_deviceId, m_uri;
    boost::uuids::uuid m_uuid;
    Monitor::UpdatePolicy m_upolicy;
};





}

#endif // include guard
