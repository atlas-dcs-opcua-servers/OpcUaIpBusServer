
/*
 * DataScript.h
 *
 * Created on: August, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

/* This is device header stub */


#ifndef __DDataScript__H__
#define __DDataScript__H__

#include <vector>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>
#include <externalmonitor.h>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DDataScript.h>

class Monitor;
namespace boost { namespace python { struct object; } }

namespace Device
{


class
    DDataScript
    : public Base_DDataScript
{

public:
    /* sample constructor */
    explicit DDataScript ( const Configuration::DataScript & config

                           , DDevice * parent

                         ) ;
    /* sample dtr */
    ~DDataScript ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DDataScript( const DDataScript & ) = delete;
    DDataScript& operator=(const DDataScript &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void init(Monitor* monitor);
    void destroy();
    void storeOutputVariable( std::string name, unsigned int value );
	std::vector<std::string> getOutputVariables () const;

private:
    void run();

    std::string m_path, m_funcName;
    boost::python::object* m_func;
    Monitor* m_monitor;

    boost::shared_ptr<ExternalMonitor> m_emonitor;
    boost::thread m_thr;
};





}

#endif // include guard
