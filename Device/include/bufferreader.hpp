/*
 * bufferreader.h
 *
 * Created on: July 6, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

#ifndef BUFFERREADER_H
#define BUFFERREADER_H

#include <generalbuffer.hpp>
#include <monitor.hpp>
#include <string>

template <typename T> class GeneralBuffer;
template <typename T> class GBAccessInterface;
class Monitor;

/**
 * BufferReader's interface class
 * used providing special access to the methods for the
 * privileged Monitor and GeneralBuffer classes
 */
template <typename T>
class BRAccessInterface
{
	friend class Monitor;
	template <typename K> friend class GeneralBuffer;

public:
	virtual ~BRAccessInterface() {}

protected:
	virtual void addBuffer(int id, GeneralBuffer<T>* buff) = 0;
	virtual void setControl(boost::mutex* mutex, boost::condition_variable* cond, bool* ok) = 0;
	virtual bool isControlInitialized() = 0;

	virtual void invalidate(GeneralBuffer<T>* buffer) = 0;
};

/**
 * BufferReader class
 * any consumer that wants to subscribe to a buffer must inherit this class
 */
template <typename T>
class BufferReader : public BRAccessInterface<T>
{

public:
	BufferReader(Monitor* monitor = NULL);
	virtual ~BufferReader() {}

protected:
	/**
	 * Read methods used by the consumer to get data from the buffer.
	 */
	/**
	 * Read method: uses local to return a vector (with an entry for each
	 * subscription) of vectors (with an entry for each data point in the
	 * respective buffer)
	 * This method copies the data found in the buffers
	 * @param local destination structure for outputing read data from the
	 * monitored buffers
	 */
	int read(std::vector< std::vector<T> >& local);
	/**
	 * This read method does not copy the data from the buffers.
	 * It returns direct pointers to the data inside the buffers.
	 * @param amounts vector of lengths for the data found in each monitored buffer
	 * @param arrays vector of pointers to the actual data found inside a buffer
	 */
	int read(std::vector<int>& amounts, std::vector<const T*>& arrays);
	/**
	 * When using the no-copy read method, the endRead methos should be called
	 * to notify the buffer that this reader has finished its reading routine
	 * and it can advance to the next portion of incoming data
	 * @param amounts vector of lenghts for the data read from each monitored buffer
	 */
	void endRead(const std::vector<int>& amounts);

	/**
	 * Method used by the consumer to subscribe to a register of an ipbus device.
	 * @param name name of the register
	 * @return the index of the subscription which tells the position in the read
	 * vectors of the incoming data.
	 */
	int subscribe(const std::string& name);
	/**
	 * Sets the monitor class which manages subscriptions.
	 * @param monitor pointer to the monitor class
	 */
	void setMonitor(Monitor* monitor);
	/**
	 * Callback called when the reader has been unsubscribed due to an error.
	 */
	virtual void notifyInvalidation(int idx) {}

	Monitor* m_monitor;

private:
	/**
	 * Method used by the protected reading methods to start reading.
	 * It provides a vector of lengths for the data available in each monitored buffer.
	 * It does this taking care of synchronization with other consumers and the monitoring thread.
	 * @param amounts vector of lengths
	 */
	int beginRead(std::vector<int>& amounts);

	/**
	 * Method called by a buffer to notify the reader that it has been given an ID
	 * inside the buffer.
	 * @param id ID of the reader inside the monitored buffer buff
	 * @param buff the buffer that called this method
	 */
	void addBuffer(int id, GeneralBuffer<T>* buff);
	/**
	 * Method called by a buffer to notify the reader that its subscription ID has changed
	 * @param id old ID
	 * @param newId new ID
	 */
	void subscriptionUpdate(int id, int newId);
	/**
	 * Method called by the monitor to offer the reader pointers to the control variables
	 * @param mutex common mutex
	 * @param cond common condition variable
	 * @param ok boolean variable by which the reader knows if the monitor is dead or alive
	 */
	void setControl(boost::mutex* mutex, boost::condition_variable* cond, bool* ok);
	/**
	 * Checks if the control variables have been already initialized
	 * @return return true or false
	 */
	bool isControlInitialized();
	/**
	 * Method called by a buffer to notify the reader that it has been unsubscribed
	 * due to an error
	 * @param buffer buffer that called this method
	 */
	void invalidate(GeneralBuffer<T>* buffer);

	bool* m_writerEnabled;
	boost::mutex* m_mutex;
	boost::condition_variable* m_cond;
	boost::mutex m_localMutex;

	std::vector<int> m_ids;
	std::vector< GeneralBuffer<T>* > m_buffers;
};

template <typename T>
BufferReader<T>::BufferReader(Monitor* monitor)
	: m_monitor(monitor)
	, m_writerEnabled(NULL)
	, m_mutex(NULL)
	, m_cond(NULL)
{

}

template <typename T>
int BufferReader<T>::beginRead(std::vector<int>& amounts)
{
	int maximum = 0;

	boost::unique_lock<boost::mutex> guard(*m_mutex);
	while (true) {
		maximum = 0;
		amounts.clear();

		for (unsigned int i = 0; i < m_buffers.size(); i++) {
			amounts.push_back(static_cast<GBAccessInterface<T>*>(m_buffers[i])->getCount(m_ids[i]));
			maximum = std::max(maximum, amounts[i]);
		}

		if (maximum == 0 && *m_writerEnabled)
			m_cond->wait(guard);
		else
			break;
	}

	return maximum;
}

template <typename T>
int BufferReader<T>::read(std::vector< std::vector<T> >& local)
{
	std::vector<int> amounts;
	amounts.reserve(local.size());

	int err = beginRead(amounts);
	if (err <= 0)
		return err;

	m_localMutex.lock();
	for (unsigned int i = 0; i < m_buffers.size(); i++)
		static_cast<GBAccessInterface<T>*>(m_buffers[i])->read(m_ids[i], local[i], amounts[i]);
	m_localMutex.unlock();

	endRead(amounts);
	return 1;
}

template <typename T>
void BufferReader<T>::endRead(const std::vector<int>& amounts)
{
	boost::lock_guard<boost::mutex> guard(*m_mutex);

	for (unsigned int i = 0; i < m_buffers.size(); i++)
		static_cast<GBAccessInterface<T>*>(m_buffers[i])->advance(m_ids[i], amounts[i]);
}

template <typename T>
int BufferReader<T>::read(std::vector<int>& amounts, std::vector<const T*>& arrays)
{
	int maximum = 0;

	boost::unique_lock<boost::mutex> guard(*m_mutex);
	while (true) {
		maximum = 0;
		for (unsigned int i = 0; i < m_buffers.size(); i++) {
			static_cast<GBAccessInterface<T>*>(m_buffers[i])->exportBuffer(m_ids[i], amounts[i], arrays[i]);
			maximum = std::max(maximum, amounts[i]);
		}

		if (maximum == 0 && *m_writerEnabled)
			m_cond->wait(guard);
		else
			break;
	}
	return maximum;
}

template <typename T>
void BufferReader<T>::addBuffer(int id, GeneralBuffer<T>* buff)
{
	m_ids.push_back(id);
	m_buffers.push_back(buff);
}

template <typename T>
void BufferReader<T>::invalidate(GeneralBuffer<T>* buffer)
{
	for (unsigned int i = 0; i < m_buffers.size(); i++)
		if (m_buffers[i] == buffer) {
			m_localMutex.lock();
			m_ids.erase(m_ids.begin() + i);
			m_buffers.erase(m_buffers.begin() + i);
			m_localMutex.unlock();

			notifyInvalidation(i);
			break;
		}
}

template <typename T>
void BufferReader<T>::setControl(boost::mutex* mutex, boost::condition_variable* cond, bool* ok)
{
	m_mutex = mutex;
	m_cond = cond;
	m_writerEnabled = ok;
}

template <typename T>
bool BufferReader<T>::isControlInitialized()
{
	return m_mutex != NULL;
}

template <typename T>
int BufferReader<T>::subscribe(const std::string& name)
{
	if (!m_monitor)
		return -1;

	m_monitor->subscribe(this, name);
	return m_ids.size() - 1;
}

template <typename T>
void BufferReader<T>::setMonitor(Monitor* monitor)
{
	m_monitor = monitor;
}

#endif
