/*
 * generalbuffer.h
 *
 * Created on: July 6, 2015
 * Author: Cristian-Valeriu Soare
 * E-mail: soare.cristian21@gmail.com
 */

#ifndef GENERALBUFFER_H 
#define GENERALBUFFER_H

#include <pthread.h>
#include <vector>
#include <bufferreader.hpp>

/**
 * GeneralBuffer's interface class
 * used for providing privileged access to BufferReader class
 */
template <typename T>
class GBAccessInterface
{
	template <typename K> friend class BufferReader;

public:
	virtual ~GBAccessInterface() {}

protected:
	virtual const int& getLeft(int id) = 0;
	virtual void read(int id, std::vector<T>& dest, int len) = 0;
	virtual int getCount(int id) = 0;
	virtual void advance(int id, int count) = 0;
	virtual void exportBuffer(int id, int& count, const T*& array) = 0;
};

template <typename T>
class GeneralBuffer : public GBAccessInterface<T>
{

public:
	/**
	 * Constructor
	 * @param cap capacity of the buffer
	 * @param mutex reference to a mutex for synchronization between buffer readers
	 * @param con conditional variable used for sync
	 * @param writerEnabled notifies the readers whether the monitor is working or not
	 */
	GeneralBuffer(int cap, boost::mutex& mutex, boost::condition_variable& cond, bool& writerEnabled);
	~GeneralBuffer();

	GeneralBuffer(const GeneralBuffer&) = delete;
	GeneralBuffer& operator=(const GeneralBuffer&) = delete;

	/**
	 * Method called by a buffer reader to request an access ID from the general buffer.
	 * @param br the buffer reader which called this method
	 */
	void requestAccessId(BufferReader<T>* br);
	/**
	 * Reset method which resets the buffer to its initial, unfilled state.
	 * @param full if it is true, it will clear any subscribed readers
	 */
	void reset(bool full = false);

	/**
	 * Method called by the writer. Takes care of synchronization.
	 * @return amount of free entries in the buffer to be filled.
	 */
	int beginWrite();
	/**
	 * Method called by the writer. Takes care of synchronization.
	 * @param val value to be inserted inside the buffer.
	 * @return if the operation completed successfully
	 */
	bool add(T val);
	/**
	 * Method called by the writer. Takes care of synchronization.
	 * Marks the end of writing procedure, notifies any subscribed readers.
	 */
	void endWrite();

protected:
	T* m_buffer;
	const int m_capacity;

	/**
	 * Method that returns the reading index in the buffer corresponding to the
	 * subscriber id.
	 * @param id ID of the subscriber
	 * @return index inside the buffer.
	 */
	const int& getLeft(int id);
	/**
	 * Method that reads data from the buffer len entries and copies them inside the dest vector.
	 * @param id ID of the reader
	 * @param dest destination vector
	 * @param len length of the read data
	 */
	void read(int id, std::vector<T>& dest, int len);
	/**
	 * Returns the amount of unread data for a reader by its ID.
	 * @param id ID of the reader
	 * @return count of unread data
	 */
	int getCount(int id);
	/**
	 * Method that confirms that count entries have been read by the consumer of ID id.
	 * @param id ID of the reader
	 * @param count count of read data
	 */
	void advance(int id, int count);
	/**
	 * Method that offers a consumer a pointer to the inner buffer and the length of the
	 * data that is to be read.
	 * @param id ID of the reader
	 * @param count count of the data to be read
	 * @param array pointer to the inner buffer
	 */
	void exportBuffer(int id, int& count, const T*& array);

private:
	/**
	 * Gets the maximum unread data by a reader.
	 * @return count
	 */
	int maxCount();
	/**
	 * Gets the unread data count, being provided with the left limit of the buffer.
	 * @param left left limit of the buffer
	 * @return count of unread data entries
	 */
	int getCountLeft(int left);

	int m_right, m_ongoingRight, m_writeLimit;
	std::vector<int> m_lefts;
	std::vector<BufferReader<T>*> m_readers;

	boost::mutex& m_mutex;
	boost::condition_variable m_hasSpace, &m_hasElements;
	bool& m_writerEnabled;
};

template <typename T>
GeneralBuffer<T>::GeneralBuffer(int cap, boost::mutex& mutex, boost::condition_variable& cond, bool& writerEnabled)
	: m_capacity(cap + 1)
	, m_mutex(mutex)
	, m_hasElements(cond)
	, m_writerEnabled(writerEnabled)
{
	m_buffer = new T[m_capacity];
	m_writeLimit = 0;
	m_right = m_ongoingRight = 0;
}

template <typename T>
GeneralBuffer<T>::~GeneralBuffer()
{
	delete[] m_buffer;
}

template <typename T>
void GeneralBuffer<T>::requestAccessId(BufferReader<T>* br)
{
	m_lefts.push_back(0);
	m_readers.push_back(br);
	static_cast<BRAccessInterface<T>*>(br)->addBuffer(m_lefts.size() - 1, this);
}

template <typename T>
void GeneralBuffer<T>::reset(bool full)
{
	m_right = m_ongoingRight = 0;
	for (unsigned int i = 0; i < m_lefts.size(); i++)
		m_lefts[i] = 0;
	if (full)
		m_lefts.clear();
}

template <typename T>
int GeneralBuffer<T>::getCountLeft(int left)
{
	if (left <= m_right)
		return m_right - left;
	return m_capacity - left + m_right;
}

template <typename T>
int GeneralBuffer<T>::getCount(int id)
{
	return getCountLeft(m_lefts[id]);
}

template <typename T>
const int& GeneralBuffer<T>::getLeft(int id)
{
	return m_lefts[id];
}

template <typename T>
int GeneralBuffer<T>::beginWrite()
{
	boost::unique_lock<boost::mutex> guard(m_mutex);
	int count;

	while ((count = maxCount()) == m_capacity - 1)
		m_hasSpace.wait(guard);

	m_ongoingRight = m_right;
	m_writeLimit = m_capacity - count - 1;

	return m_writeLimit;
}

template <typename T>
bool GeneralBuffer<T>::add(T val)
{
	if (!m_writeLimit)
		return false;

	m_buffer[m_ongoingRight] = val;
	m_ongoingRight = (m_ongoingRight + 1) % m_capacity;
	m_writeLimit--;

	if (!m_writeLimit)
		endWrite();
	return true;
}

template <typename T>
void GeneralBuffer<T>::endWrite()
{
	boost::lock_guard<boost::mutex> guard(m_mutex);
	if (m_right != m_ongoingRight) {
		m_right = m_ongoingRight;
		m_hasElements.notify_all();
	}
	m_writeLimit = 0;
}

template <typename T>
void GeneralBuffer<T>::advance(int id, int count)
{
	m_lefts[id] = (m_lefts[id] + count) % m_capacity;
}

template <typename T>
void GeneralBuffer<T>::exportBuffer(int id, int& count, const T*& array)
{
	count = (m_lefts[id] <= m_right ? m_right : m_capacity) - m_lefts[id];
	array = m_buffer + m_lefts[id];
}

template <typename T>
void GeneralBuffer<T>::read(int id, std::vector<T>& dest, int len)
{
	int left = m_lefts[id];
	for (int i = 0; i < len; i++, left = (left + 1) % m_capacity)
		dest.push_back(m_buffer[left]);
}

template <typename T>
int GeneralBuffer<T>::maxCount()
{
	if (m_lefts.empty())
		return 0;

	int maximum = 0;
	for (unsigned int i = 0; i < m_lefts.size(); i++) {
		int count = getCountLeft(m_lefts[i]);

		if (count == m_capacity - 1) {
			((BRAccessInterface<T>*)m_readers[i])->invalidate(this);
			m_lefts.erase(m_lefts.begin() + i);
			m_readers.erase(m_readers.begin() + i);
			count = 0;
			i--;
		}

		if (maximum < count)
			maximum = count;
	}
	return maximum;
}

#endif // GENERALBUFFER_H
