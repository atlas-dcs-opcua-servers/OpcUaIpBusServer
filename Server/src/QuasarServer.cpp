/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QuasarServer.h"
#include <LogIt.h>
#include <string.h>
#include <shutdown.h>
#include <pyembed.h>
#include <boost/foreach.hpp>
#include <DDevice.h>
#include <LogItComponentIDs.h>

QuasarServer::QuasarServer() : BaseQuasarServer()
{
    PyEmbed::getInstance();
}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
		UaThread::sleep (1);
    }
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing Quasar server for IpBus device logic.";
    BOOST_FOREACH (Device::DDevice* device,
        Device::DRoot::getInstance()->devices()) device->init(); 
}

void QuasarServer::shutdown()
{
    LOG(Log::INF) << "Shutting down Quasar server.";
    BOOST_FOREACH (Device::DDevice* device,  Device::DRoot::getInstance()->devices())
        device->destroy();
    PyEmbed::destroyInstance();

}

void QuasarServer::initializeLogIt()
{
    Log::initializeLogging(Log::INF,
                           std::list<ComponentAttributes>(1,
                                                          ComponentAttributes(LogItComponentID::PyEmbed, "Python Log")));

    LOG(Log::INF) << "Logging initialized.";
}
