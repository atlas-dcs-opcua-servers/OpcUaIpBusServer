#OPC UA Toolkit
SET( OPCUA_TOOLKIT_PATH "/opt/OpcUaToolkit-1.3.3" )
SET( OPCUA_TOOLKIT_LIBS_DEBUG "-luamoduled -lcoremoduled -luabased -luastackd -luapkid -lxmlparserd -lxml2 -lssl -lcrypto -lpthread" ) 
SET( OPCUA_TOOLKIT_LIBS_RELEASE "-luamodule -lcoremodule -luabase -luastack -luapki -lxmlparser -lxml2 -lssl -lcrypto -lpthread" ) 

#Boost
SET( BOOST_PATH_HEADERS "/opt/cactus/include" )
SET( BOOST_PATH_LIBS "/opt/cactus/lib" )
SET( BOOST_LIBS "-lboost_python -lboost_filesystem  -lboost_regex  -lboost_system  -lboost_program_options")

#XML
SET( XML_LIBS "-lxerces-c" ) 

#Other options

add_definitions(-std=gnu++0x -Wall)
 
